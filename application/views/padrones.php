<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="padrones">SIC</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Sesi&oacute;n: <a href="#" class="navbar-link"><?php echo $user_data['name']?></a>
            </p>
            <ul class="nav">
              <li class="active"><a href="padrones">Inicio</a></li>
              <li><a href="#about">Acerca de</a></li>
            <!--    <li><a href="#contact">Contact</a></li>  -->  
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

     <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Datos del Contribuyente</li>
              <li class="nav-header">Estado de Cuenta</li>
              <li class="nav-header">Pagos</li>
              <li class="nav-header">Cambio de Contrase&ntilde;a</li>
              <li class="nav-header">Chat en L&iacute;nea</li>
        <!--  <li class="nav-header">Sidebar</li>
              <li><a href="#">Link</a></li>
              <li class="nav-header">Sidebar</li>
              <li><a href="#">Link</a></li>
               -->  
            </ul>
          </div>
        </div>  
        
         <div class="span9">
        
	        <section id="fluidGridSystem">
	          <div class="page-header">
	            <h1>Padrones</h1>
	          </div>
	
	          
		          <div class="bs-docs-grid">
		            <div class="row-fluid show-grid">
		              <div class="span4">
		                  <h2>Tenencia</h2>
			              <p></p>
			              <p><a class="btn" href="#">Detalles &raquo;</a></p>
			          </div>
		              <div class="span4">
		                  <h2>N&oacute;mina</h2>
			              <p> </p>
			              <p><a class="btn" href="#">Detalles &raquo;</a></p>
		              </div>
		              <div class="span4">
		              	  <h2>Predial</h2>
		              	  <p></p>
		              	  <p><a class="btn" href="#">Detalles &raquo;</a></p>
		              </div>
		            </div>
		            <div class="row-fluid show-grid">
		            	<div class="span4">
			              <h2>Espect&aacute;culos P&uacute;blicos</h2>
			              <p></p>
			              <p><a class="btn" href="<?php echo base_url('espectaculos/espectaculos/vista')?>">Detalles &raquo;</a></p>
			            </div><!--/span-->
		            	<div class="span4">
			              <h2>Mercados</h2>
			              <p></p>
			              <p><a class="btn" href="#">Detalles &raquo;</a></p>
			            </div><!--/span-->
		            	<div class="span4">
			              <h2>Loter&iacute;as</h2>
			              <p></p>
			              <p><a class="btn" href="#">Detalles &raquo;</a></p>
			            </div><!--/span-->
		            </div>
		          </div>
		       
	        </section>
        
          
          
        </div>
      </div><!--/row-->

      <hr>

      <footer>
        <p></p>
      </footer>

    </div><!--/.fluid-container-->
