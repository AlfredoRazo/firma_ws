<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Espect&aacute;culos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url('css/bootstrap.css')?>" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="<?php echo base_url('css/bootstrap-responsive.css')?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('img/apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('img/apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('img/apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('img/apple-touch-icon-57-precomposed.png')?>">
    <link rel="shortcut icon" href="<?php echo base_url('img/favicon.png')?>">


	<!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?php echo base_url('js/grid/jquery/jquery-1.8.0.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('js/grid/jquery/jquery-1.8.0.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('js/grid/jquery/jquery-ui.js')?>" type="text/javascript"></script>
    
    <script src="<?php echo base_url('js/jsboot/bootstrap-transition.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-alert.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-modal.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-dropdown.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-scrollspy.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-tab.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-tooltip.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-popover.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-button.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-collapse.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-carousel.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-typeahead.js')?>"></script>
    <script src="<?php echo base_url('js/jsboot/bootstrap-datetimepicker.min.js')?>"></script>
    
    <script src="<?php echo base_url('js/ajaxfileupload.js');?>" type="text/javascript"></script>
    
    <!-- Script necesary for grid -->
	
	
	
	
	<script src="<?php echo base_url('js/grid/jquery.validate.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/flexigrid.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/flexigrid.js')?>" type="text/javascript"></script>
	<!--  <script src="<?php echo base_url('js/grid/calendar/jquery.ui.core.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/calendar/jquery.ui.datepicker.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/calendar/jquery-ui.multidatespicker.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/calendar/jquery-ui-timepicker-addon.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('js/grid/validation.js')?>" type="text/javascript"></script>-->
	
	<script src="<?php echo base_url('js/grid/modern_calendar/jquery.datetimepicker.js')?>" type="text/javascript"></script>
	
	
	
	<script src="<?php echo base_url('js/funciones.js')?>" type="text/javascript"></script>
	
	<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#fecha_ini').datepicker({
                	dateFormat: "dd-mm-yy"
                });

                $('#fecha_fin').datepicker({
                	dateFormat: "dd-mm-yy"
                });

                $('#datetimepicker1').datetimepicker({
                    pickDate: false
                  });

                $('#datetimepicker2').datetimepicker({
                    pickDate: false
                  });

            });

     </script>
	
	<script type="text/javascript">

	</script>
	<!-- Style necesary for grid -->
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/grid/flexigrid.css')?>" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/grid/flexigrid.pack.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery-ui.css')?>" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/grid/validation.css')?>" />
	<!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/grid/calendar/jquery-ui-timepicker-addon.css')?>" />--> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap-datetimepicker.min.css')?>" /> 
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/grid/modern_calendar/jquery.datetimepicker.css')?>" />
    
    <style type="text/css">
	    @media only screen and (max-width: 480px){
	        .emailButton{
	            max-width:600px !important;
	            width:100% !important;
	        }
	
	        .emailButton a{
	            display:block !important;
	            font-size:18px !important;
	        }
	    }
	</style>
    
  </head>
  
  <body>

