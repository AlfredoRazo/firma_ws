<!-- Login -->

<!-- Le styles -->
<link href="<?php echo base_url('css/bootstrap.css')?>" rel="stylesheet">
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 40px;
	background-color: #f5f5f5;
}

.form-signin {
	max-width: 300px;
	padding: 19px 29px 29px;
	margin: 0 auto 20px;
	background-color: #fff;
	border: 1px solid #e5e5e5;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.form-signin .form-signin-heading,.form-signin .checkbox {
	margin-bottom: 10px;
}

.form-signin input[type="text"],.form-signin input[type="password"] {
	font-size: 16px;
	height: auto;
	margin-bottom: 15px;
	padding: 7px 9px;
}

</style>


<link href="<?php echo base_url('css/bootstrap-responsive.css')?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url('img/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url('img/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url('img/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed"
	href="<?php echo base_url('img/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" href="img/favicon.png">


<!--  -->


<script type="text/javascript">

$(function() {
	$('#fileupload').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload({
			url 			:'<? echo base_url() . 'login/login/security'; ?>', 
			secureuri		:false,
			dataType		: 'json',
			fileElementId	:'fileToUploadCer',
			data: { 'usuario' : $('#usuario').val() , 'password' : $('#password').val() },
			success	: function (data, status)
			{
				var obj = jQuery.parseJSON(JSON.stringify(data));
				if(obj[0]['error'] != null){
					alert(obj[0]['mensaje']);
				}else{
					
				}				

				if(obj[0]['success']!=null && obj[0]['success']==true){
					alert(obj[0]['mensaje']);
					 window.location.assign("<?php echo base_url().'admin/admin/padrones'?>")
				}
				
			}
		});
		return false;
	});
});

</script>

<div class="container">

	<form class="form-signin" id="fileupload" method="post" >
			
				<h2 class="form-signin-heading">Ingrese su Firma</h2>
					Archivo .cer <input type="file" name="fileToUploadCer" id="fileToUploadCer" class="input-block-level"> 
					Usuario<input type="text" name="usuario" id="usuario" class="input-block-level"> 
					Contrase&ntilde;a<input type="text" name="password" id="password" class="input-block-level"> 
			<!-- 	<input type="checkbox" value="remember-me">
					Recordar Firma -->
				</label>
				<button class="btn btn-large btn-primary" type="submit">Ingresar</button>
				<button class="btn btn-large btn-primary" type="submit">Administardor</button>
	</form>
	
</div>