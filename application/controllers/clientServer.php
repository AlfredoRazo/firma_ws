<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ClientServer extends CI_Controller{

	var $KEY = "hUgpOw2IgnPms1PFHU2I/v5vPgk4UwDr";
	
	var $PATH = '/var/www/html/gomartinez/firma_electronica/uploads/558420150318025634/';
	
	/**
	
	@brief Ejemplos consumo firma electronica <br>
	@brief ClientServer <br>
	
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	
	*/
	
	
	/**
	
	@brief Ejemplo que verifica la llave publica <br>
	
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	
	
	Ejemplo:
	\verbatim
	
		public function index(){
			$this->load->library("nusoap_lib");
			$file = $this->PATH."cer.cer";
			
			$handle = fopen($file, "r");                  // Open the temp file
			$contents = fread($handle, filesize($file));  // Read the temp file
			fclose($handle);                                 // Close the temp file
			
			$servidor = "http://10.1.65.188/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
			$client = new nusoap_client($servidor);		
			$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);		
			$arregloResultado =  $client->call('getCertificateVerifying', $pregunta);
			
			$error = $client->getError();
			if ($error)
			{
				echo '<pre style="color: red">Error 0:' . $error . '</pre>';
				echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
						ENT_QUOTES).'</p>';
				die();
			}
			
			echo "<pre>";
			print_r($arregloResultado);
			
		} 
	
	\endverbatim
	
	
	*/
	
	public function index(){
		$this->load->library("nusoap_lib");
		$file = $this->PATH."cer.cer";
		
		$handle = fopen($file, "r");                  // Open the temp file
		$contents = fread($handle, filesize($file));  // Read the temp file
		fclose($handle);                                 // Close the temp file
		
		$servidor = "http://10.1.130.21/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
		echo file_get_contents($servidor, FILE_TEXT, stream_context_create(array('http' => array('timeout' => 1))), 0, 1);  
		$client = new nusoap_client($servidor);		
		$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);
		$arregloResultado =  $client->call('getCertificateVerifying', $pregunta);
		$error = $client->getError();
		if ($error)
		{
			echo '<pre style="color: red">Error 0:' . $error . '</pre>';
			echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
					ENT_QUOTES).'</p>';
			//die();
		}
		
		echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
		echo "<pre>";
		print_r($arregloResultado);
		
	} 

	/**
	
	
	@brief Ejemplo Obtiene los datos del certificado en cadena <br>
	
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	
	Ejemplo:
	 \verbatim
	
		public function dataCertificateComplete(){
			$this->load->library("nusoap_lib");
			$file = $this->PATH."cer.cer";
		
			$handle = fopen($file, "r");                  // Open the temp file
			$contents = fread($handle, filesize($file));  // Read the temp file
			fclose($handle);                                 // Close the temp file
		
			$servidor = "http://10.1.65.188/firma_electronica/serverFirma/serverFirma?wsdl";
			$client = new nusoap_client($servidor);
			$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);
			$arregloResultado =  $client->call('getDataCetificateComplete', $pregunta);
		
			$error = $client->getError();
			if ($error)
			{
				echo '<pre style="color: red">Error 0:' . $error . '</pre>';
				echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
						ENT_QUOTES).'</p>';
				die();
			}
		
			echo "<pre>";
			print_r($arregloResultado);
		
		}
	
	 \endverbatim
	
	*/
	
	public function dataCertificateComplete(){
		$this->load->library("nusoap_lib");
		$file = $this->PATH."cer.cer";
	
		$handle = fopen($file, "r");                  // Open the temp file
		$contents = fread($handle, filesize($file));  // Read the temp file
		fclose($handle);                                 // Close the temp file
	
		$servidor = "http://10.1.130.21/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
		$client = new nusoap_client($servidor);
		$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);
		$arregloResultado =  $client->call('getDataCetificateComplete', $pregunta);
	
		$error = $client->getError();
		if ($error)
		{
			echo '<pre style="color: red">Error 0:' . $error . '</pre>';
			echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
					ENT_QUOTES).'</p>';
			die();
		}
	
		echo "<pre>";
		print_r($arregloResultado);
	
	}
	
	/**
	
	
	@brief Ejemplo Obtiene los datos del certificado en arreglo <br>
	
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	
	Ejemplo:
	 \verbatim
	
		public function dataCetificateSubject(){
			$this->load->library("nusoap_lib");
			$file = $this->PATH."cer.cer";
		
			$handle = fopen($file, "r");                  // Open the temp file
			$contents = fread($handle, filesize($file));  // Read the temp file
			fclose($handle);                                 // Close the temp file
		
			$servidor = "http://10.1.65.188/firma_electronica/serverFirma/serverFirma?wsdl";
			$client = new nusoap_client($servidor);
			$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);
			$arregloResultado =  $client->call('getDataCetificateSubject', $pregunta);
		
			$error = $client->getError();
			if ($error)
			{
				echo '<pre style="color: red">Error 0:' . $error . '</pre>';
				echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
						ENT_QUOTES).'</p>';
				die();
			}
		
			echo "<pre>";
			print_r($arregloResultado);
		
			
		}
	
	 \endverbatim
	
	*/
	
	public function dataCetificateSubject(){
		$this->load->library("nusoap_lib");
		$file = $this->PATH."cer.cer";
	
		$handle = fopen($file, "r");                  // Open the temp file
		$contents = fread($handle, filesize($file));  // Read the temp file
		fclose($handle);                                 // Close the temp file
	
		$servidor = "http://10.1.130.21/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
		$client = new nusoap_client($servidor);
		$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY);
		$arregloResultado =  $client->call('getDataCetificateSubject', $pregunta);
	
		$error = $client->getError();
		if ($error)
		{
			echo '<pre style="color: red">Error 0:' . $error . '</pre>';
			echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
					ENT_QUOTES).'</p>';
			die();
		}
	
		echo "<pre>";
		print_r($arregloResultado);
	
		
	}
	
	/**
	
	
	@brief Ejemplo Crea el sello digital <br>
	
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	
	Ejemplo:
	 \verbatim
	
		public function buildSelloDigital(){
			$this->load->library("nusoap_lib");
			$file = $this->PATH."Clave.key";
			$file_cer = $this->PATH."cer.cer";
			$pass = "password";
			$cadenaOriginal = 'hola mundo';
			
			
			$handle = fopen($file, "r");                  // Open the temp file
			$contents = fread($handle, filesize($file));  // Read the temp file
			fclose($handle);                                 // Close the temp file
		
			$handle = fopen($file_cer, "r");                  // Open the temp file
			$contents_cer = fread($handle, filesize($file_cer));  // Read the temp file
			fclose($handle);
			
			
			$servidor = "http://10.1.65.188/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
			$client = new nusoap_client($servidor);
			$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY,'cer'=>$this->mCrypt($contents_cer),'password'=>$this->mCrypt($pass),'cadenaOriginal'=>$cadenaOriginal);
			$arregloResultado =  $client->call('buildSelloDigital', $pregunta);
		
			$error = $client->getError();
			if ($error)
			{
				echo '<pre style="color: red">Error 0:' . $error . '</pre>';
				echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
						ENT_QUOTES).'</p>';
				die();
			}
		
			echo "<pre>";
			print_r($arregloResultado);
		
	}
	
	 \endverbatim
	
	
	*/
	
	public function buildSelloDigital(){
		$this->load->library("nusoap_lib");
		$file = $this->PATH."Clave.key";
		$file_cer = $this->PATH."cer.cer";
		$pass = "";
		$cadenaOriginal = 'hola mundo';
		
		
		$handle = fopen($file, "r");                  // Open the temp file
		$contents = fread($handle, filesize($file));  // Read the temp file
		fclose($handle);                                 // Close the temp file
	
		$handle = fopen($file_cer, "r");                  // Open the temp file
		$contents_cer = fread($handle, filesize($file_cer));  // Read the temp file
		fclose($handle);
		
		
		$servidor = "http://10.1.130.21/gomartinez/firma_electronica/serverFirma/serverFirma?wsdl";
		$client = new nusoap_client($servidor);
		$pregunta = array('file'=>$this->mCrypt($contents),'key'=>$this->KEY,'cer'=>$this->mCrypt($contents_cer),'password'=>$this->mCrypt($pass),'cadenaOriginal'=>$cadenaOriginal);
		$arregloResultado =  $client->call('buildSelloDigital', $pregunta);
	
		$error = $client->getError();
		if ($error)
		{
			echo '<pre style="color: red">Error 0:' . $error . '</pre>';
			echo '<p style="color:red;'>htmlspecialchars($client->getDebug(),
					ENT_QUOTES).'</p>';
			die();
		}
	
		echo "<pre>";
		print_r($arregloResultado);
	
	}
	
	/**
	 @brief Encode
	 @returns String Encode
	 */
	
	private function mCrypt($plaintext = ''){
		 
		$key = $this->KEY;
		 
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		 
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,$plaintext, MCRYPT_MODE_CBC, $iv);
		 
		$ciphertext = $iv . $ciphertext;
		 
		$ciphertext_base64 = base64_encode($ciphertext);
		 
		return $ciphertext_base64;
		 
	}
	
	/**
	 @brief Decode
	 @returns String Decode
	 */
	
	private function dCrypt($ciphertext_base64){
		 
		$key = $this->KEY;
		 
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		 
		$ciphertext_dec = base64_decode($ciphertext_base64);
		 
		$iv_dec = substr($ciphertext_dec, 0, $iv_size);
		 
		$ciphertext_dec = substr($ciphertext_dec, $iv_size);
		 
		$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,$ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
		 
		return $plaintext_dec;
	}
	
}


?>