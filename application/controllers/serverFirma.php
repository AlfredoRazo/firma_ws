<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ServerFirma extends CI_Controller{
	
	/**
	 
	  
	  @brief Web Service de firma electronica <br>
	  
	  @author Guillermo Omar Martinez Toledo
  	  @date 18/04/2015, 25/04/2015
	  
	  @brief libreria nusoap <br>
	  
	 */
	
	function ServerFirma() {
	
		parent::__construct();
			
		$this->load->library("nusoap_lib");
			
		$this->server = new soap_server();
		$this->server->configureWSDL("ServerFirma", "urn:firmawsdl");
		$this->server->wsdl->schemaTargetNamespace = "urn:firmawsdl";
			
		$this->server->wsdl->addComplexType(
				'TWsRespuesta',
				'complexType',
				'struct',
				'all',
				'',
				array(
						'respuesta' => array('name' => 'respuesta', 'type' => 'xsd:string')
				)
		);
	
			
		$this->server->register(
				'getCertificateVerifying'
				, array ('file' => 'xsd:string','key'=>'xsd:string')
				, array('return' => 'tns:TWsRespuesta')
				, 'urn:firmawsdl'
				, 'urn:firmawsdl#getCertificateVerifying'
				, 'rpc'
				, 'encoded'
				, "get Signatura Check."
		);
		
		$this->server->register(
				'getDataCetificateComplete'
				, array ('file' => 'xsd:string','key'=>'xsd:string')
				, array('return' => 'tns:TWsRespuesta')
				, 'urn:firmawsdl'
				, 'urn:firmawsdl#getDataCetificateComplete'
				, 'rpc'
				, 'encoded'
				, "get Data Certificate Complete."
		);
		
		$this->server->register(
				'getDataCetificateSubject'
				, array ('file' => 'xsd:string','key'=>'xsd:string')
				, array('return' => 'tns:TWsRespuesta')
				, 'urn:firmawsdl'
				, 'urn:firmawsdl#getDataCetificateSubject'
				, 'rpc'
				, 'encoded'
				, "get Data Certificate Complete Array By Person."
		);
			
	
		$this->server->register(
				'buildSelloDigital'
				, array ('file' => 'xsd:string','key'=>'xsd:string','cer'=>'xsd:string','password'=>'xsd:string','cadenaOriginal'=>'xsd:string')
				, array('return' => 'tns:TWsRespuesta')
				, 'urn:firmawsdl'
				, 'urn:firmawsdl#buildSelloDigital'
				, 'rpc'
				, 'encoded'
				, "get Build Digital Stamp."
		);
		
		/**
		   @brief Metodo Web Service getCertificateVerifying <br>
		   @brief Usa Model_firma_electronica <br>
		   @brief Verifica la llave publica del contribuyente <br>
		   @brief Verifica si la llave publica es vigente <br>
		   @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error <br>
		   @returns JSON result 
		*/
		
		function getCertificateVerifying($file,$key){
			$CI =& get_instance();
			$CI->load->model('model_firma_electronica/model_firma_electronica');
			$mFirmaElectronica = new Model_firma_electronica();
			return array('respuesta'=>json_encode($mFirmaElectronica->revisaSATNoSave($file,$key)));
		}

		/**
		 @brief Metodo Web Service getDataCetificateComplete <br>
		 @brief Usa Model_firma_electronica <br>
		 @brief Obtiene los datos de la llave publica <br>
		 @brief Retorna el valor en cadena <br>
		 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error <br>
		 @returns JSON result
		 */
		
		function getDataCetificateComplete($file,$key){
			$CI =& get_instance();
			$CI->load->model('model_firma_electronica/model_firma_electronica');
			$mFirmaElectronica = new Model_firma_electronica();
			return array('respuesta'=>json_encode($mFirmaElectronica->getDataCetificateComplete($file,$key)));
		}
		
		/**
		 @brief Metodo Web Service getDataCetificateSubject <br>
		 @brief Usa Model_firma_electronica <br>
		 @brief Obtiene los datos de la llave publica del contribuyente <br>
		 @brief Retorna el valor en arreglo <br>
		 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error <br>
		 @returns JSON result
		 */
		
		function getDataCetificateSubject($file,$key){
			$CI =& get_instance();
			$CI->load->model('model_firma_electronica/model_firma_electronica');
			$mFirmaElectronica = new Model_firma_electronica();
			return array('respuesta'=>json_encode($mFirmaElectronica->getDataCetificateSubject($file,$key)));
		}
		
		/**
		 @brief Metodo Web Service buildSelloDigital <br>
		 @brief Usa Model_firma_electronica <br>
		 @brief Crea el sello digital <br>
		 @brief Regresa la cadena sellada <br>
		 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error <br>
		 @returns JSON result
		 */
		
		function buildSelloDigital($file,$key,$cer,$password,$cadenaOriginal){
			$CI =& get_instance();
			$CI->load->model('model_firma_electronica/model_firma_electronica');
			$mFirmaElectronica = new Model_firma_electronica();
			return array('respuesta'=>json_encode($mFirmaElectronica->buildSelloDigital($file,$key,$cer,$password,$cadenaOriginal)));
		}
		
		$HTTP_RAW_POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
			
		$this->server->service($HTTP_RAW_POST_DATA);
			
	}
	
	
	
	function index(){
		if($this->uri->segment(3) == "wsdl")
			$_SERVER['QUERY_STRING'] = "wsdl";
		else
			$_SERVER['QUERY_STRING'] = "";
			
	}
	
	
	
}
