<?php 
class Model_form extends CI_Model{
		var $form;
        var $name;
        var $id;
       	var $arrayElement;
        var $action;
        var $method;
        
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
       public function setId($id){
       		$this->id = " id ='" .$id."' ";
       }

       public function getId(){
       		return $this->id;
       }
       
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }

        public function buildForm(){
        	$this->form.= $this->getId() . $this->getName(). $this->getMethod() . $this->getAction() . '>';
        	foreach ($this->arrayElement as $value){
        		$this->form.= $value;
        	}
        	
        	
        	
        	$this->form.= '</form>';
        	
        	
        }
        
        public function getForm(){
        	return $this->form;
        }
        
        public function setAction($action){
        	$this->action = " action= '".$action."' ";
        }
        
        public function getAction(){
        	return $this->action;
        }
        
        public function setMethod($method){
        	$this->method = " method= '".$method."'";
        }
        
        public function getMethod(){
        	return $this->method;
        }
        
        public function addElement($element){
        	$this->arrayElement[] = $element;
        }
        
        public function initElements(){
        	$this->form = ' <form ';
        	$this->id = '';
        	$this->name = '';
        	$this->action = '';
        	$this->method = '';
        	$this->arrayElement = array();
        }
        
}
?>