<?php 
class Model_a_post extends CI_Model{
		var $a_post;
        var $id;
        var $action;
        var $method;
        var $arrayElement;
        var $arrayInput;
        var $onclick;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }

        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setAction($action){
        	$this->action = $action;
        }
        
        public function getAction(){
        	return $this->action;
        }
        
        public function setMethod($method){
        	$this->method = $method;
        }
        
        public function getMethod(){
        	return $this->method;
        }
        
		 public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
        public function addInput($value){
        	$this->arrayInput[] = $value;
        }
        
        public function setOnclick($onclick){
        	$this->onclick = $onclick;
        }
        
        public function buildA_post(){
        	$this->load->model('grid/model_form/model_form');
        	$this->load->model('grid/model_a/model_a');
        	$form = new Model_form();
        	$form->setAction($this->getAction());
        	$form->setMethod($this->getMethod());
        	$model_a = new Model_a();
        	$model_a->setHref('javascript:;');
        	$model_a->setOnclick($this->onclick);
        	foreach ($this->arrayElement as $value){
        		$model_a->addElement($value);
        	}
        	$model_a->buildA();
        	$form->addElement($model_a->getA());
        	foreach ($this->arrayInput as $value){
        		$form->addElement($value);
        	}
        	$form->buildForm();
        	$this->a_post = $form->getForm();
        }
        
        public function getA_post(){
        	return $this->a_post;
        }
        
        public function getLabel(){
        	return $this->label;
        }
        
        public function initElements(){
        	$this->a_post = '';
        	$this->id = '';
        	$this->name = '';
        	$this->action = '';
        	$this->method = '';
        	$this->arrayElement = array();
        	$this->arrayInput = array();
        }
        
}
?>