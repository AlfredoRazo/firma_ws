<?php 
/***
 * Model_engine hereda las propiedades de Model_engine_sql
 * Model_engine_sql dinamic query
 * v1.0
 * **/
class Model_engine extends Model_engine_sql {
	
		var $ERROR; /*Error*/
		var $ARRAYCOLUMNS; /*Data of columns*/
		var $SHOW_NEW = true; /*Show Button New*/
		var $SHOW_EDIT = true; /*Show edit link*/
		var $SHOW_POPUP_CONTENT = false; /*Show POPUP CONTENT FUNCTION*/
		var $SHOW_POPUP_CONTENT_URL = '';/*URL Function*/
		var $SHOW_POPUP_CONTENT_TEXT = '';/*Text Colum*/
		var $SHOW_DELETE = true /*Show delete link*/;
		var $SHOW_CONTENT = false; /*Show Content of view*/
		var $SHOW_CONTENT_URL = ''; /*URL show Content*/
		var $SHOW_CONTENT_TEXT = '';
		var $CONTROLLER;/*Controller*/
		var $PATH;/*path grid*/
		var $TITLE = 'Resultado';/*table title*/
		var $ID = 'table'; /*Id table*/
		var $SERVERRUTE = 'serverProcessing'; /*server json data*/
		var $SHOW_TOGGLEBTN = false;
		var $HEIGHT = 400; /*Define height Grid*/
		var $VARS_GENERATE_PAGINATION_GRID = array(); /*Vars To Back Page*/
		var $SHOW_BACK_BUTTON = false; /*Show back button*/
		
		var $NAME_GRID_SESSION = '';
		
		function __construct() {
            parent::__construct();
        }
        
        function initFuntions(){
        	$this->prepareStack();
        }
        
        public function setShowBackButton($SHOW_BACK_BUTTON = false){
        	$this->SHOW_BACK_BUTTON = $SHOW_BACK_BUTTON;
        	$this->VARS_GENERATE_PAGINATION_GRID = $_POST;
        }
        
        public function getShowBackButton(){
        	return $this->SHOW_BACK_BUTTON;
        }
        
        public function getVarsGenerateGridPaginationGrid(){
        	return $this->VARS_GENERATE_PAGINATION_GRID;
        }
        
        public function setHeight($value = 400){
        	$this->HEIGHT = $value;
        }
        
        public function getHeight(){
        	return $this->HEIGHT;
        }
        
        public function sIdTable($id){
        	$this->ID = $id;
        }
        
        public function sSERVERRUTE($serverRute = 'serverProcessing'){
        	$this->SERVERRUTE = $serverRute;
        }
        
        public function sTitle($title = 'Resultado'){
        	$this->TITLE = $title;
        }
        
        public function sToggleBTN($show_togglebtn = true){
        	$this->SHOW_TOGGLEBTN = $show_togglebtn;
        }
        
        public function publishGridInSession($name){
        	
        	if(!isset($_SESSION)) 
		         session_start(); 
		    
        	$user_data = $_SESSION;
        	
        	if(isset($user_data['grid_session'])){
        		
        		$grid_session = $user_data['grid_session'];
        		
        		$nGrids = count($grid_session);
        		
        		$nameGridSession = 'GridSession'.$name;
        		
        		$grid_session[$nameGridSession] = $this->mCrypt(serialize($this));
        		
        		$_SESSION['grid_session'] = $grid_session;
        		
        	}else{
        		
        		$grid_session = array();
        		
        		$nameGridSession = 'GridSession'.$name;
        		
        		$grid_session[$nameGridSession] = $this->mCrypt(serialize($this));
        		
        		$_SESSION['grid_session'] = $grid_session;
        		
        	}
        	$this->setNameGridSession($nameGridSession);
        	
        }
        
        public function setNameGridSession($nameGridSession){
        	$this->NAME_GRID_SESSION = $nameGridSession;
        }
        
        /*
         * Generate Key
         * */
        
        private function cKey(){
        	$this->load->model('grid/model_password_hash/model_password_hash');
        	$mPH = new Model_password_hash();
        	$key = $mPH->create_hash('i82dtmg41');
        	//echo $key.'  ';
        	//echo $mPH->validate_password('i82dtmg41', $key);
        }
        
        /*
         * Encode
         * */
        
        private function mCrypt($plaintext = 'ejemplo'){
        	
        	$key = 'hUgpOw2IgnPms1PFHU2I/v5vPgk4UwDr';
        	
        	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        	
        	$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,$plaintext, MCRYPT_MODE_CBC, $iv);
        	
        	$ciphertext = $iv . $ciphertext;
        	
        	$ciphertext_base64 = base64_encode($ciphertext);
        	
        	return $ciphertext_base64;
        	
        }
        
        /*
         * Decode
         * */
        
        private function dCrypt($ciphertext_base64){
        	
        	$key = 'hUgpOw2IgnPms1PFHU2I/v5vPgk4UwDr';
        	
        	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        	
        	$ciphertext_dec = base64_decode($ciphertext_base64);
        	
        	$iv_dec = substr($ciphertext_dec, 0, $iv_size);
        	
        	$ciphertext_dec = substr($ciphertext_dec, $iv_size);
        	
        	$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,$ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
        	
        	return $plaintext_dec;
        }
        
        /*
         * Build table
         * */
        
       
        public function buildTable($name){
        	$this->publishGridInSession($name);
        	$this->load->model('grid/model_table/model_table');
        	$tabla = new Model_table();
        	$tabla->setId($name);
        	$tabla->buildTable();
        	$html = $tabla->getTable();
        	return $html;
        }
        
        /*
         * Build flex
         * */
       
        
        public function buildFlex($name){
        	$colModel = array('colModel' => array());
        	
        	$this->load->helper('url');
        	
        	$this->where('ROWNUM<=1');
        	
        	if($this->ERRORSQL){
        		$this->select();
	        	
	        	$result = $this->gResult();
	        	
	        	if(!empty($result)){
	        		 
	        		$this->setColumns($result);
	        		
	        		if($this->SHOW_POPUP_CONTENT)
	        			$colModel['colModel'][] = array('display' => $this->SHOW_POPUP_CONTENT_TEXT ,'name'=> 'ver','width'=>100,'sortable'=>false,'align'=>'left');
	        		if($this->SHOW_CONTENT)
	        			$colModel['colModel'][] = array('display' => $this->SHOW_CONTENT_TEXT ,'name'=> 'ver','width'=>100,'sortable'=>false,'align'=>'left');
	        		if($this->SHOW_DELETE)
	        			$colModel['colModel'][] = array('display' => 'Eliminar','name'=> 'delete','width'=>50,'sortable'=>false,'align'=>'left');
	        		if($this->SHOW_EDIT)
	        			$colModel['colModel'][] = array('display' => 'Editar','name'=> 'edit','width'=>50,'sortable'=>false,'align'=>'left');
	        		
	        		
	        		foreach ($this->ARRAYCOLUMNS as $key => $value){
	        				$hide_column = false;
		        			if(isset($this->PROPERTIES[$key]['HIDE_COLUMN'])){
		        					$hide_column = $this->PROPERTIES[$key]['HIDE_COLUMN'];
		        			}
		        			$colModel['colModel'][] = array('display' => $this->ARRAYCOLUMNS[$key]['DISPLAY'],'name'=> $this->ARRAYCOLUMNS[$key]['name'],'width'=>($this->ARRAYCOLUMNS[$key]['max_length']*2)+100,'sortable'=>true,'align'=>'left','hide'=>$hide_column);
		        			if(!$hide_column)
		        				$colModel['searchitems'][] = array('display' => $this->ARRAYCOLUMNS[$key]['DISPLAY'],'name'=> $this->ARRAYCOLUMNS[$key]['name']);
		        			
	        		}
	        		
	        		$function = 'doCommand';
	        		
	        		
	        		
	        		if(!$this->SHOW_TOGGLEBTN)
	        			$showToggleBtn = 'showToggleBtn: false,';
	        		else 
	        			$showToggleBtn = '';
	        		
	        		$colModel['buttons'][] = array('name'=>'Nuevo','bclass'=>'add','onpress'=>$function);
	        		
	        		$url = base_url($this->PATH.'/'.$this->SERVERRUTE);
	        		$flex = '$("#'.$name.'").flexigrid({'.$showToggleBtn.'url:"'.$url.'",dataType:"json",';
	        		$flex.= 'colModel:'.json_encode($colModel['colModel']);
	        		$flex.= ',searchitems:'.json_encode($colModel['searchitems']);
	        		
	        		if($this->SHOW_NEW)
	        			$flex.= ',buttons:[{name:"Nuevo",bclass:"add",onpress:doCommand}]';
	        		
	        		$flex.= ',showTableToggleBtn: true,title: "'.$this->TITLE.'",useRp: true,rp: 20,resizable: true,singleSelect: true,usepager: true,height:'.$this->getHeight().',usepager: true});';
	        		
	        		$form = 'var form = $(\'<form action = "'.base_url($this->PATH."/create").'" id="'.'create'.$this->ID.'" method="post"><input type="hidden" name="name_grid_session" id="name_grid_session" value="'.$this->ID.'"/></form>\'); $(\'body\').append(form); target_popup'.$this->ID.'(document.forms["create'.$this->ID.'"]);  $( "#create'.$this->ID.'" ).remove(); ';
	        		
	        		$comand = 'function doCommand(com,grid){'.$form.'}';
	        		
	        		return $flex.$comand;
	        		 
	        	}else{
	        		$this->ERROR['ERROR_NUMBER'] = '';
	        		$this->ERROR['ERROR_MSG'] = 'La Tabla no existe en la base de datos';
	        	}
        	}else{
        		$this->ERROR['ERROR_NUMBER'] = '';
        		$this->ERROR['ERROR_MSG'] = 'La Tabla no existe en la base de datos';
        	}
        	
        }
        
        
        /*
         * Name column of the table
         * */
        
        public function setColumns($gResult){
        	
        	$general = $gResult['general']['COLUMNS'];
        
        	foreach ($gResult as $key => $rValue){
        		
        		if(is_numeric($key)){
	        			$data = $this->gNTableNoValidations($rValue['TABLE_NAME']);
	        			if(!empty($data)){
		        			foreach ($data as $value){
		        				$array = get_object_vars($value);
		        				if(isset($this->PROPERTIES[strtoupper($value->name)]['DISPLAY']))
		        					$array['DISPLAY'] = $this->PROPERTIES[strtoupper($value->name)]['DISPLAY'];
		        				else 
		        					$array['DISPLAY'] = strtoupper($value->name);
		        				
		        				if(isset($general[strtoupper($value->name)]))
		        				$array['name'] = $general[strtoupper($value->name)]->name;

		        				if(!isset($rValue['CATALOG']))
		        					$this->ARRAYCOLUMNS[strtoupper($value->name)] = $array;
		        			}
		        		}else{
	        				$this->ERROR['ERROR_NUMBER'] = '';
	        				$this->ERROR['ERROR_MSG'] = 'La Tabla no existe en la base de datos';
	        			}
        		}
        	}
        	$this->setColumnsComplement();
        }
        
        public function setColumnsComplement(){
        
        if(isset($this->COLUMNS_NAME_COMPLEMENT))
        	foreach ($this->COLUMNS_NAME_COMPLEMENT as $valueC){
        		
        		$array['name'] = strtoupper($valueC);
        		
        		if(isset($this->PROPERTIES[strtoupper($valueC)]['DISPLAY']))
        			$array['DISPLAY'] = $this->PROPERTIES[strtoupper($valueC)]['DISPLAY'];
        		else
        			$array['DISPLAY'] = strtoupper($valueC);

        		if(isset($this->PROPERTIES[strtoupper($valueC)]['MAX_LENGTH']))
        			$array['max_length'] = $this->PROPERTIES[strtoupper($valueC)]['MAX_LENGTH'];
        		else
        			$array['max_length'] = 22;
        		
        			$array['ONLY_INFO'] = true;
        		
        		$this->ARRAYCOLUMNS[$valueC] = $array;
        		
        		
        	}
        }
        
        /*
         * Gets the name of the columns in the table if no validation
         * */
        
        private function gNTableNoValidations($table_name){
        	$field_data = $this->DB->field_data($table_name);
        	return $field_data;
        }
   
        /*
         * Get data server
         */
        
        public function serverProcessing($_GET){
        
        	$_GET['iDisplayStart'] = (( $_GET['page'] - 1 ) * $_GET['rp']);
        	$_GET['iDisplayLength'] = $_GET['page'] * $_GET['rp'];
        	 
        	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
        		$this->rowNum(($_GET['iDisplayStart']+1),($_GET['iDisplayLength']));
        	}
        
        	$aColumns = $this->gResult();
        	$aColumns = $aColumns['general']['COLUMNS'];
        	$aKeys = array_keys($aColumns);
        
        	$sOrder = "";
        	
        	if ( isset( $_GET['sortname'] ) && strlen(trim($_GET['sortname']))>0  && trim($_GET['sortname'])!='undefined'){
        		$sOrder = "ORDER BY  ";
        		$sOrder .= $_GET['sortname'];
        		
        		if(isset($_GET['sortorder']) && strlen(trim($_GET['sortorder']))>0 && trim($_GET['sortorder'])!='undefined'){
        			$sOrder.= " " .$_GET['sortorder'];
        		}
        		
        		if ( $sOrder == "ORDER BY  " )
        		{
        			$sOrder = "";
        		}
        		
        	}
        	
        	$parameter = array();
        	
        	if(isset($_GET['qtype']) && is_array($_GET['qtype'])){
	        	if(isset( $_GET['qtype']) && count($_GET['qtype'])>0){
	        		if(isset( $_GET['query']) && count($_GET['query'])>0){
	        			
	        			for($i = 0;$i<count($_GET['query']);$i++){
	        				$qtype = $_GET['qtype'][$i];
	        				$query = $_GET['query'][$i];
	        				$parameter[$qtype] = $query;
	        			}
	        			
	        			
	        		}
	        	}
        	}else{
      			if(isset( $_GET['qtype']) && strlen(trim($_GET['qtype']))>0){
      				if(isset( $_GET['query']) && strlen(trim($_GET['query']))>0){
      					$parameter[$_GET['qtype']] = $_GET['query'];
      				}
      			}
      		}
	        	
        	$this->order($sOrder);
        	
        	$this->select($parameter);
        
        	$result = $this->gResult();
        
        	/*
        	 * SQL queries
        	* Get data to display
        	*/
        
        	$query = $result['general']['select'];
        	

        	$iData = $this->DB->query($query)->result_array();
        	$iFilteredTotal = count($iData);
        
        
        	/* Total data set length */
        	$query = $result['general']['count'];
        	$cData = $this->DB->query($query)->result_array();
        	$cData = $cData[0]['COUNT'];
        	$iTotal = $cData;
        
        	/*
        	 * output
        	*/
        
        	$output = array(
        			"total" => $iTotal,
        			"page"=>$_GET['page'],
        			"rows" => array()
        	);
        
        	$columns = $result['general']['COLUMNS'];
        	
        	$output = $this->buildRow($iData, $output,$columns);
        	
        	return $output;
        }        
        
        
        public function buildRow($iData,$output,$columns){
        	foreach ($iData as $iValue){
        		$row = array();
        		if($this->SHOW_EDIT)
        			array_unshift($iValue,$this->buildLink($iValue,$columns,'edit'));
        		if($this->SHOW_DELETE)
        			array_unshift($iValue,$this->buildLink($iValue,$columns,'delete'));
        		if($this->SHOW_CONTENT)
        			array_unshift($iValue,$this->buildLink($iValue,$columns,'content'));
        		if($this->SHOW_POPUP_CONTENT)
        			array_unshift($iValue,$this->buildLink($iValue,$columns,'popup_content'));
        		 
        		foreach ($iValue as $key => $rValue){
        			 
        			/*Add value to record*/
        			if(isset($this->PROPERTIES[$key]['FORCE_ADD_VALUE_COMPLEMENT'])){
        				$rValue.=$this->PROPERTIES[$key]['FORCE_ADD_VALUE_COMPLEMENT'];
        			}else if(isset($this->PROPERTIES[$key]['FORCE_ADD_VALUE'])){
        				$rValue = $this->PROPERTIES[$key]['FORCE_ADD_VALUE'];
        			}
        			 
        			$row[] = $rValue;
        		}
        	
        		$output['rows'][] = array('cell'=>$row);
        	}
        	
        	return $output;
        }
        
        /*Create form link*/
        
        public function buildLink($iValue,$columns,$action){
        	 unset($iValue['RNUM']);
        	 
        	 /*Create form method post*/
        	 
        	 $this->load->model('grid/model_a_post/model_a_post');
        	 $this->load->model('grid/model_input/model_input');
        	 
        	 if($action == 'edit')
        	 	$url =  $this->config->base_url() . $this->PATH . '/edit';
        	 else if($action == 'delete')
        	 	$url =  $this->config->base_url() . $this->PATH . '/delete';
        	 else if($action == 'content')
        	 	$url =  $this->config->base_url() . $this->SHOW_CONTENT_URL;
        	 else if($action == 'popup_content')
        	 	$url =  $this->config->base_url() . $this->SHOW_POPUP_CONTENT_URL;
        	 
        	 $a_post = new Model_a_post();
        	 $a_post->setAction($url);
        	 $a_post->setMethod('post');
        	 
        	 if($action == 'edit'){
        	 	$a_post->addElement('Editar');
        	 	$a_post->setOnclick('target_popup'.$this->ID.'(parentNode)');
        	 } else if($action == 'delete'){
        	 	$a_post->addElement('Eliminar');
        	 	$a_post->setOnclick('target_popupDelete'.$this->ID.'(parentNode);');
        	 }else if($action == 'content'){
        	 	$a_post->addElement($this->SHOW_CONTENT_TEXT);
        	 	$a_post->setOnclick('show_content'.$this->ID.'(parentNode)');
        	 }else if($action == 'popup_content'){
        	 	$a_post->addElement($this->SHOW_POPUP_CONTENT_TEXT);
        	 	$a_post->setOnclick('target_popupPOPUPCONTENT'.$this->ID.'(parentNode)');
        	 }
        	 
        	 /*Add input in form*/
        	 
        	 foreach ($iValue as $key => $valueColumn){
        	 	if(!is_numeric($key))
        	 	if(!empty($valueColumn) && isset($columns[$key])){
	        	 	$input = new Model_input();
	        	 	$input->setType('hidden');
	        	 	$input->setName(str_replace('.','-',$columns[$key]->name));
	        	 	$input->setValue($valueColumn);
	        	 	$input->buildInput();
	        	 	$a_post->addInput($input->getInput());
        	 	}
        	 }
        	 
        	 /*Serealize object name*/
        	 
        	 $input = new Model_input();
        	 $input->setType('hidden');
        	 $input->setName('name_grid_session');
        	 $input->setValue($this->ID);
        	 $input->buildInput();
        	 $a_post->addInput($input->getInput());
        	 
        	 $a_post->buildA_post();
        	 return $a_post->getA_post();
        }
        
        /*
         * Code base 64
         * */
        
        function base64url_encode($data) {
        	return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        }
        
        /*
         * Decode base 64
         * */
        
        function base64url_decode($data) {
        	return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        }
        
        public function showNEW($visible = true){
        	$this->SHOW_NEW = $visible;
        }
        
        public function showDELETE($visible = true){
        	$this->SHOW_DELETE = $visible;
        }
        
        public function showEDIT($visible = true){
        	$this->SHOW_EDIT = $visible;
        }
        
        public function showCONTENT($visible = true,$url,$show_content_text = 'Ver'){
        	$this->SHOW_CONTENT = $visible;
        	$this->SHOW_CONTENT_URL = $url;
        	$this->SHOW_CONTENT_TEXT = $show_content_text;
        }
        
        public function showPOPUPCONTENT($visible = true,$url,$show_popup_content_text = 'Ver'){
        	$this->SHOW_POPUP_CONTENT = $visible;
        	$this->SHOW_POPUP_CONTENT_URL= $url;
        	$this->SHOW_POPUP_CONTENT_TEXT = $show_popup_content_text;
        }
        
        /*
         * Edit Row
         * Get Controller Controler.php
         * */
       
        public function edit($post){
        	
        	/*Object Session Active*/
        	
        	if(!isset($_SESSION)) 
		     	session_start(); 
        	
        	$name_grid_session = $post['name_grid_session'];
        	
        	$grid_session_var = $_SESSION['grid_session']['GridSession'.$name_grid_session];
        	
        	/*
        	 * Object serealize
        	 * */
        	
        	$serialize = $grid_session_var;
        	unset($post['security']);
        	
        	/*
        	 * Decode
        	 * */
        	
        	$dcrypt = $this->dCrypt($serialize);
        	
        	
        	/*
        	 * Unserialize Object
        	 * */
        	
        	$object = unserialize($dcrypt);
        	
        	/*
        	 * Set vars Serealizer
        	 * */
        	$this->GGLOBAL = $object->GGLOBAL;
        	$this->TABLE_NAME = $object->TABLE_NAME;
        	$this->DB = $object->DB;
        	$this->PARENT = $object->PARENT;
        	$this->PROPERTIES = $object->PROPERTIES;
        	$this->PATH = $object->PATH;
        	$this->TITLE = $object->TITLE;
        	
        	/*Get Foreign keys general array*/
        	
        	if(isset($this->TABLE_NAME['general']['FOREIGN_KEY']))
        		$foreign_key = $this->TABLE_NAME['general']['FOREIGN_KEY'];
        	else
        		$foreign_key = array();
        	
        	/*
        	 * Set parameters select of row
        	 * */
        	
        	$postKey = array();
        	foreach ($post as $key => $value){
        		$nKey = str_replace('-','.',$key);
        		if(array_key_exists($nKey,$foreign_key))
         			$postKey[$nKey] = $value;
        	}
        	
        	$this->select($postKey);
        	
        	$result = $this->gResult();
        	
        	/*
        	 * SQL queries
        	* Get data to display
        	*/
        	
        	$query = $result['general']['select'];

        	$iData = $this->DB->query($query)->result_array();
        	
        	if(!empty($iData))
        		$this->gEValue($iData[0]);
        	
        	$this->setColumns($result);
        	
        	return $this->gForm($iData,$result,'_action_update',$name_grid_session);
        	
        }
        
        
        public function delete($post){
        	 
        	if(!isset($_SESSION)) 
		         session_start(); 
        	
        	$name_grid_session = $post['name_grid_session'];
        	
        	$grid_session_var = $_SESSION['grid_session']['GridSession'.$name_grid_session];
        	
        	/*
        	 * Object serealize
        	 * */
        	
        	$serialize = $grid_session_var;

        	unset($post['security']);
        	 
        	/*
        	 * Decode
        	* */
        	 
        	$dcrypt = $this->dCrypt($serialize);
        	 
        	 
        	/*
        	 * Unserialize Object
        	* */
        	 
        	$object = unserialize($dcrypt);
        	 
        	/*
        	 * Set vars Serealizer
        	* */
        	
        	$this->GGLOBAL = $object->GGLOBAL;
        	$this->TABLE_NAME = $object->TABLE_NAME;
        	$this->DB = $object->DB;
        	$this->PARENT = $object->PARENT;
        	$this->PROPERTIES = $object->PROPERTIES;
        	$this->PATH = $object->PATH;
        	$this->TITLE = $object->TITLE;
        	/*Get Foreign keys general array*/
        	
        	if(isset($this->TABLE_NAME['general']['FOREIGN_KEY']))
        		$foreign_key = $this->TABLE_NAME['general']['FOREIGN_KEY'];
        	else
        		$foreign_key = array();
        	
        	/*
        	 * Set parameters select of row
        	 * */
        	
        	$postKey = array();
        	foreach ($post as $key => $value){
        		$nKey = str_replace('-','.',$key);
        		if(array_key_exists($nKey,$foreign_key))
         			$postKey[$nKey] = $value;
        	}
        	
        	Model_engine_sql::delete($postKey);
        
        	foreach (array_reverse($this->TABLE_NAME) as $valueT){
        		if(isset($valueT['delete'])){
        			$query = $valueT['delete'];
        			echo $query;
        			if(!$this->DB->query($query)){
        				$this->ERROR['ERROR_NUMBER'] = $this->db->_error_number();
        				$this->ERROR['ERROR_MSG'] = $this->db->_error_message();
        			}
        		}
        	
        	}
        }
        
        
        /*
         * Create Row
        * Get Controller Controler.php
        * */
         
        public function create($post){

        	if(!isset($_SESSION)) 
		         session_start(); 
        	 
        	$name_grid_session = $post['name_grid_session'];
        	 
        	$grid_session_var = $_SESSION['grid_session']['GridSession'.$name_grid_session];
        	 
        	/*
        	 * Object serealize
        	* */
        	 
        	$serialize = $grid_session_var;
        	 
        	unset($post['security']);

        	
        	/*
        	 * Decode
        	* */
        	 
        	$dcrypt = $this->dCrypt($serialize);
        	 
        	 
        	/*
        	 * Unserialize Object
        	* */
        	 
        	$object = unserialize($dcrypt);
        	 
        	/*
        	 * Set vars Serealizer
        	* */
        	$this->GGLOBAL = $object->GGLOBAL;
        	$this->TABLE_NAME = $object->TABLE_NAME;
        	$this->DB = $object->DB;
        	$this->PARENT = $object->PARENT;
        	$this->PROPERTIES = $object->PROPERTIES;
			$this->PATH = $object->PATH;
			$this->TITLE = $object->TITLE;
			
        	$this->select();
        	 
        	$result = $this->gResult();
        	
        	/*
        	 * SQL queries
        	* Get data to display
        	*/
        	 
        	$query = $result['general']['select'];
        	 
        	$iData = $this->DB->query($query)->result_array();
        	 
        	$this->setColumns($result);

        	$parameters = $this->gSequence();
        	
        	$this->gSValue($parameters);
        	
        	return $this->gForm($parameters,$result,'_create_new',$name_grid_session);
        	 
        }
        
        public function gSequence(){
        	$parameters = array();
        	if(!empty($this->PROPERTIES))
        	foreach ($this->PROPERTIES as $key => $pValues){
        		if(isset($pValues['SEQUENCE'])){
        			$sequence = $pValues['SEQUENCE'];
        			$dSequence = $this->DB->query($sequence);
        			if(!empty($dSequence)){
        				$dSequence = $dSequence->result_array();
        			    $parameters[] = array($key=>$dSequence[0][$key]);
        			}
        		}
        	}
        	return $parameters;
        }
        
        public function gSValue(&$parameters){
        	if(!empty($this->PROPERTIES))
        		foreach ($this->PROPERTIES as $key => $pValues){
        			if(isset($pValues['SVALUE']))
        				$parameters[] = array($key=>$pValues['SVALUE']);
        		}
        }
        
        public function gEValue(&$iData){
        	if(!empty($this->PROPERTIES))
        		foreach ($this->PROPERTIES as $key => $pValues){
        		if(isset($pValues['EVALUE']))
        			$iData[$key] = $pValues['EVALUE'];
        	}
        }
        
        /*Create form for edit and create new row*/
        
        private function gForm($iData = array(),$result = null,$type = '_create_new',$name_grid_session=''){
        	$sSelect = array();
        	
        	foreach ($result as $key => $value){
        		if(is_numeric($key) && $key>0){
        			if(isset($value['CATALOG']))
	        			if(isset($value['PRIMARY_KEY'])){
	        				$pk = '';
	        				foreach ($value['PRIMARY_KEY'] as $valuePK){
	        					$pk.= $valuePK;
	        					break;
	        				}
	        				$sSelect[$pk] = $value['select'];
	        			}
        		}
        	}
        	        	
        	$this->load->model('grid/model_div/model_div');
        	$this->load->model('grid/model_label/model_label');
        	$this->load->model('grid/model_fieldset/model_fieldset');
        	$this->load->model('grid/model_input/model_input');
        	$this->load->model('grid/model_form/model_form');
        	$this->load->model('grid/model_select/model_select');
        	$this->load->model('grid/model_article/model_article');
        	
        	$script = '<script> $(document).ready(function() { $("#gridForm").validate();';
        	
        	
        	if (!empty($this->ARRAYCOLUMNS)){
        		
        		$form = new Model_form();
        		$form->setId('gridForm');
        		$mDiv = new Model_div();
        		$mDiv->setClass('module_content');
        		foreach ($this->ARRAYCOLUMNS as $key => $column){
        			
        			if(!isset($column['ONLY_INFO'])){
        			
	        			if(!isset($this->PROPERTIES[$key]['HIDDEN']) || (isset($this->PROPERTIES[$key]['HIDDEN']) && $this->PROPERTIES[$key]['HIDDEN'] == false))
	        				$script.=$this->functionType($column['type'],$key);
	        			
	        			if(isset($this->PROPERTIES[$key]['EXPRESION']) && strlen($this->PROPERTIES[$key]['EXPRESION'])>1){
	        				if(isset($this->PROPERTIES[$key]['ERROR_MESSAGE_EXPRESION']))
	        					$script.=$this->functionValidate($this->PROPERTIES[$key]['EXPRESION'], $key,$this->PROPERTIES[$key]['ERROR_MESSAGE_EXPRESION']);
	        				else
	        					$script.=$this->functionValidate($this->PROPERTIES[$key]['EXPRESION'], $key);
	        			}
	        			
	        			if(isset($this->PROPERTIES[$key]['FUNCION']) && strlen($this->PROPERTIES[$key]['FUNCION'])>1){
	        				if(isset($this->PROPERTIES[$key]['ERROR_MESSAGE_FUNCION']))
	        					$script.=$this->functionValidateFuncion($this->PROPERTIES[$key]['FUNCION'], $key,$this->PROPERTIES[$key]['ERROR_MESSAGE_FUNCION']);
	        				else
	        					$script.=$this->functionValidateFuncion($this->PROPERTIES[$key]['FUNCION'], $key);
	        			}
	        			
	        			
	        			$element = $this->gType($sSelect,$key,$column,$iData);
	        			
	        			$mDiv->addelement($element);
        			}
        		}
        		
        		$mDiv->buildDiv();
        		
        		/*$article = new Model_article();
        		$article->setClass('module width_full');
        		$article->addElement('<header><h3>'.$this->TITLE.'</h3></header>');
        		$article->addElement($mDiv->getDiv());
        		        		
        		$input = new Model_input();
        		$input->setType('submit');
        		
        		if($type == '_create_new'){
        			$input->setName('_create_new');
        			$input->setValue('Crear');
        		}else{
        			$input->setName('_action_update');
        			$input->setValue('Guardar');
        		}
        		
        		$input->setClass('save');
        		
        		$input->setId('create');
        		$input->buildInput();
        		        		
        		$article->addElement('<footer><div class="submit_link"> '.$input->getInput().'</div></footer>');
        		$article->buildArticle();
        		$form->addElement($input->getInput());*/
        		
        		$form->addElement($mDiv->getDiv());
        		
        		$input = new Model_input();
        		$input->setType('hidden');
        		$input->setId('name_grid_session');
        		$input->setName('name_grid_session');
		        $input->setValue($name_grid_session);
        		
        		$input->buildInput();
        		$form->addElement($input->getInput());
        		
        		if($type == '_create_new'){
        			$url = $this->config->base_url() . $this->PATH . '/save';
        		}else{
        			$url = $this->config->base_url() . $this->PATH . '/update';
        		}
        		$script.=' }); </script>';
        		
        		$form->addElement($script);
        		
        		$form->setAction($url);
        		$form->setMethod('post');
        		$form->buildForm();
        		
        		return $form->getForm();
        	}
        	
        }
        
        /*
         * Get type element html
         * */
        
        private function gType($sSelect,$key,$column,$iData){
        	if(isset($this->PROPERTIES[$key]['SELECT'])  && !isset($this->PROPERTIES[$key]['HIDDEN'])){
        		return $this->gSelectABased($key, $this->PROPERTIES[$key]['SELECT'], $column,$iData);
        	}else if(array_key_exists($key,$sSelect)  && !isset($this->PROPERTIES[$key]['HIDDEN'])){
        		return $this->gSelectCBased($key, $sSelect, $iData, $column);
        	}else if(isset($this->PROPERTIES[$key]['HIDDEN']) && $this->PROPERTIES[$key]['HIDDEN'] == true){
	        	return $this->gInputHidden($key, $iData, $column);
        	}else{
        		return $this->gInput($key, $iData, $column);
        	}
        }
        
        /*
         * build input
         * */
        
        private function gInput($key,$iData,$column){
        	$label = new Model_label();
        	$label->setFor($key);
        	$label->addElement($column['DISPLAY']);
        	$label->buildLabel();
        	
        	$input = new Model_input();
        	$input->setType('text');
        	$input->setId($key);
        	$input->setName($key);
        	
        	if($column['type']=='DATE')
        		$input->setMaxlength(10);
        	else if(strpos($column['type'],'TIMESTAMP') !== false)
        		$input->setMaxlength(16);
        	else if(isset($column['max_length']) && $column['type']!='DATE')
        		$input->setMaxlength($column['max_length']);
        	 
        	foreach ($iData as $iKey => $value){
        		if(isset($iData[$iKey][$key])){
        			$input->setValue($iData[$iKey][$key]);
        		}
        	}
        	$input->buildInput();
        	
        	$fieldset = new Model_fieldset();
        	//$fieldset->setClass('form');
        	$fieldset->addElement($label->getLabel() . $input->getInput());
        	$fieldset->buildFieldset();
        	
        	return $fieldset->getFieldSet();
        }
        
        /*
         * build input hidden
         * */
        
        private function gInputHidden($key,$iData,$column){
        	
        	$input = new Model_input();
        	$input->setType('hidden');
        	$input->setId($key);
        	$input->setName($key);
        	
        	if($column['type']=='DATE')
        		$input->setMaxlength(10);
        	else if(strpos($column['type'],'TIMESTAMP') !== false)
        		$input->setMaxlength(16);
        	else if(isset($column['max_length']) && $column['type']!='DATE')
        		$input->setMaxlength($column['max_length']);
        	 
        	foreach ($iData as $iKey => $value){
        		if(isset($iData[$iKey][$key])){
        			$input->setValue($iData[$iKey][$key]);
        		}
        	}
        	
        	$input->buildInput();
        	return $input->getInput();
        	
        }
        
        /*
         * Select catalog based
        * */
        
        private function gSelectCBased($key,$sSelect,$iData,$column){
        	$result = $this->fSelect($sSelect[$key],$key);
        	$select = new Model_select();
        	
        	$select->setId($key);
        	$select->setName($key);
        	
        	foreach ($result as $value){
        		$oVal = '';
        		$keyVal = '';
        		$cVal = '';
        		foreach ($value as $okey => $oValue){
        			$oVal.= $oValue . '|';
        			$keyVal.=  $okey.'='.$oValue . '|';
        			if(isset($iData[0][$okey]))
        				$cVal.= $iData[0][$okey]. '|';
        		}
        		$oVal = substr($oVal, 0, -1);
        		$keyVal = substr($keyVal, 0, -1);
        		$cVal = substr($cVal, 0, -1);
        		$selection = explode('|', $oVal);
        		$selected = explode('|', $cVal);
        		if($selection[0] == $selected[0])
        			$select->openOption($keyVal,$oVal,'selected');
        		else
        			$select->openOption($keyVal,$oVal,'');
        	}
        	
        	$select->closeOption();
        	$select->buildSelect();
        	
        	$label = new Model_label();
        	$label->setFor($key);
        	$label->addElement($column['DISPLAY']);
        	$label->buildLabel();
        	
        	$fieldset = new Model_fieldset();
        	$fieldset->addElement($label->getLabel() . $select->getSelect());
        	$fieldset->buildFieldset();
        	return $fieldset->getFieldSet();
        }
        
        
        /*
         * Select array based
         * */
        
        private function gSelectABased($key,$array,$column,$iData = array()){
        	
        	$select = new Model_select();
        	$select->setId($key);
        	$select->setName($key);

        	foreach ($array as $aKey => $value){
        		
        		if(empty($iData)){
        			$select->openOption($aKey,$value,'');
        		}else{
        			if(isset($iData[0][$key]) && $iData[0][$key] == $value)
        				$select->openOption($aKey,$value,'selected');
        			else
        				$select->openOption($aKey,$value,'');
        		}
        		
        	}
        	
        	$select->closeOption();
        	$select->buildSelect();
        	 
        	$label = new Model_label();
        	$label->setFor($key);
        	$label->addElement($column['DISPLAY']);
        	$label->buildLabel();
        	 
        	$fieldset = new Model_fieldset();
        	$fieldset->addElement($label->getLabel() . $select->getSelect());
        	$fieldset->buildFieldset();
        	return $fieldset->getFieldSet();
        	
        }
        
        /*
         * Result element type select
         * */
        
        private function fSelect($query,$key){
        	return $this->DB->query($query.' ORDER BY '.$key)->result_array();
        }
       
        /*
         * Function calendar type date in element 
         * */
        
        private function functionType($type,$name){
          	if(strtoupper($type) == 'DATE'){
        			return "$('#".$name."').datetimepicker({format: 'Y-m-d',showOn: 'both',buttonImage: '".$this->config->base_url()."images/calendar.png',buttonImageOnly: false});";
        	}else if(strpos($type,'TIMESTAMP') !== false){
        		    return "$('#".$name."').datetimepicker({format: 'Y-m-d H:i',showOn: 'both',buttonImage: '".$this->config->base_url()."images/calendar.png',buttonImageOnly: false});";
        	}
        }
        
        /*
         * Function validate
         */
		
        private function functionValidate($expression,$name,$error_message='Error'){
            return "
            		$.validator.addMethod(
		        '$name',
		        function(value, element, regexpString) {
		       
		        	
		            var regParts = regexpString.match(/^\/(.*?)\/([gim]*)$/);
		            if (regParts) {
		                var regexp = new RegExp(regParts[1], regParts[2]);
		            } else {
		                var regexp = new RegExp(regstring);
		            }
		            return regexp.test(value);
		        },
		        '$error_message'
			);
			
			$('#$name').rules('add', { $name: '$expression'});
			";
        }
        
        private function functionValidateFuncion($funcion,$name,$error_message='Error'){
        	$nameFunction = $name .'_Function';
        	return "
        			$.validator.addMethod(
        	'$nameFunction',
        	function(value, element, regexpString) {
        	 		if ($funcion(element)) {
	        			return true;
	        			
	        		}
        		return false;
        		},
        		'$error_message'
         	);
        	
        	$('#$name').rules('add', { $nameFunction: ''});
        	";
        }
        
        public function sPATH($path){
        	$this->PATH = $path;
        }
        
        /*
         * Set controller name
         * */
        
        public function setController($class){
        	$this->CONTROLLER = $class;
        }
        
        /*
         * Update Row
         * Get Controller Controller.php
         * */
        
        public function update($post){
        	
        	if(!isset($_SESSION))
        		session_start();
        	
        	$name_grid_session = $post['name_grid_session'];
        	
        	$grid_session_var = $_SESSION['grid_session']['GridSession'.$name_grid_session];
        	
        	/*
        	 * Object serealize
        	* */
        	
        	$serialize = $grid_session_var;
        	
        	unset($post['security']);
        	
        	/*
        	 * Decode  
        	* */
        	 
        	$dcrypt = $this->dCrypt($serialize);
        	
        	 
        	/*
        	 * Unserialize Object
        	* */
        	 
        	$object = unserialize($dcrypt);
        	 
        	/*
        	 * Set vars Serealizer
        	* */
        	$this->GGLOBAL = $object->GGLOBAL;
        	$this->TABLE_NAME = $object->TABLE_NAME;
        	$this->DB = $object->DB;
        	$this->PARENT = $object->PARENT;
        	
        	/*
        	 * Get Update query
        	 * */
        	Model_engine_sql::update($post);
        	
        	/*
        	 * Get Parent tables
        	 * */
        	
        	foreach ($this->TABLE_NAME as $valueT){
        		if(isset($valueT['update'])){
        			$query = $valueT['update'];
        			if(!$this->DB->query($query)){
        				$this->ERROR['ERROR_NUMBER'] = $this->db->_error_number();
        				$this->ERROR['ERROR_MSG'] = $this->db->_error_message();
        			}
        		}
        		
        	}	
        }
        
        public function save($post){
        	
        	if(!isset($_SESSION)) 
		         session_start(); 
        	 
        	$name_grid_session = $post['name_grid_session'];
        	 
        	$grid_session_var = $_SESSION['grid_session']['GridSession'.$name_grid_session];
        	 
        	/*
        	 * Object serealize
        	* */
        	 
        	$serialize = $grid_session_var;
        	 
        	unset($post['security']);
        	
        	/*
        	 * Decode
        	* */
        	
        	$dcrypt = $this->dCrypt($serialize);
        	 
        	
        	/*
        	 * Unserialize Object
        	* */
        	
        	$object = unserialize($dcrypt);
        	
        	/*
        	 * Set vars Serealizer
        	* */
        	
        	$this->GGLOBAL = $object->GGLOBAL;
        	$this->TABLE_NAME = $object->TABLE_NAME;
        	$this->DB = $object->DB;
        	$this->PARENT = $object->PARENT;
        	$this->PATH = $object->PATH;
        	
        	/*
        	 * Get Insert query
        	* */
        	
        	Model_engine_sql::insert($post);
        	
        	foreach ($this->TABLE_NAME as $valueT){
        		if(isset($valueT['insert'])){
        			$data = $this->DB->query($valueT['select']);
        			if($data != null)
        				$data = get_object_vars($data);
        			else 
        				$data = array();
        			
        			if(empty($data) || (isset($data['result_array']) && empty($data['result_array']))){
        				$query = $valueT['insert'];
        				$this->DB->query($query);
        			}else{
        				$this->ERROR['ERROR_NUMBER'] = '0';
        				$this->ERROR['ERROR_MSG'] = 'No se pudo insertar, registro duplicado';
        			}
        		}
        	}
        	
        }
        
        
        public function prepareStack(){
        	
        	/*identifies whether the flow begins*/
        	
        	$basename = basename ( "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" );
        	
        	$link =  "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        	
        	$_POST['URL'] = $link;
        	$_POST['CALL_FROM'] = basename ($_SERVER['HTTP_REFERER']);
        	
        	if(isset($_POST['name_grid_session'])){

        		if(!isset($_SESSION['grid_session']['flow_start'])){
	        		$_SESSION['grid_session']['flow_start'] = true;
	        	}
	        	
	       }else{
        		
	       		if(isset($_SESSION['grid_session']['flow_start'])){
        			unset($_SESSION['grid_session']['flow_start']);
        		}

        		if(isset($_SESSION['grid_session']['flow'])){
        			unset($_SESSION['grid_session']['flow']);
        		}
        		
        	}

        	if(!isset($_SESSION['grid_session']['flow'][$basename]))
        		$_SESSION['grid_session']['flow'][$basename] = $_POST;
        	
        } 
        
        
        /*
         * Back Button
         * */
        
        
        
        public function buildBackButton(){
        
        	if(isset($_SESSION['grid_session']['flow'])){

        		$flow = $_SESSION['grid_session']['flow'];
	        	
	        	$basename = basename ( "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" );
	        	
	        	if(isset($flow[$flow[$basename]['CALL_FROM']])){
	        		$data = $flow[$flow[$basename]['CALL_FROM']];
		        	
		        	$this->load->model('grid/model_a_post/model_a_post');
		        	$this->load->model('grid/model_input/model_input');
		        	 
		        	$a_post = new Model_a_post();
		        	
		        	$a_post->setMethod('post');
		        	$a_post->setOnclick('show_content'.$this->ID.'(parentNode)');
		        	 
		        	$a_post->addElement('Volver Atr&aacute;s');
		        	
		        	$a_post->setAction($data['URL']);
		        	
		        	foreach ($data as $key => $valueColumn){
		        		$input = new Model_input();
		        		$input->setType('hidden');
		        		$input->setName(str_replace('.','-',$key));
		        		$input->setValue($valueColumn);
		        		$input->buildInput();
		        		$a_post->addInput($input->getInput());
		        	}
		        	
		        	$input = new Model_input();
		        	$input->setType('hidden');
		        	
		        	
		        	if(isset($data['name_grid_session'])){
		        		$input->setName('name_grid_session');
		        		$input->setValue($data['name_grid_session']);
		        	}else{
		        		$input->setName('name_grid_session_no_defined');
		        		$input->setValue('');
		        	}
		        	
		        	$input->buildInput();
		        	$a_post->addInput($input->getInput());
		        	 
		        	$a_post->buildA_post();
		        	
		        	return $a_post->getA_post();
	        	}
	        	return "";
        	}
        }
                
}
?>