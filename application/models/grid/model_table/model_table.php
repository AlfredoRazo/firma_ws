<?php 
class Model_table extends CI_Model{
        var $table;
        var $id;
        var $style;
        var $ArrayHead;
        var $ArrayBody;
       
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setStyle($style){
        	$this->style = " style = '".$style."'";
        }
        
        public function getStyle(){
        	return $this->style;
        }

        public function openThead(){
        	$this->ArrayHead[] = ' <thead> ';
        } 
        
        public function newTh($value,$class){
        	$this->ArrayHead[] = "<th class='".$class."' >$value</th>";
        }
        
        public function closeThead(){
        	$this->ArrayHead[] = '</thead>';
        }
        
        public function openTr(){
        	$this->ArrayBody[] = '<tr>';
        }
        
        public function closeTr(){
        	$this->ArrayBody[] = '</tr>';
        }
        
        public function newTd($value){
        	$this->ArrayBody[] = '<td>'.$value.'</td>';
        }
        
        public function buildTable(){
        	$this->table.= $this->getId() . $this->getStyle() . '>';
        	
        	foreach ($this->ArrayHead as $value){
        		$this->table.= $value;
        	}
        	$this->table.='<tbody>';
        	foreach ($this->ArrayBody as $value){
        		$this->table.= $value;
        	}
        	$this->table.='</tbody>';
        	$this->table.= '</table>';
        }

        public function getTable(){
        	return $this->table;
        }
        
        public function initElements(){
        	$this->table = ' <table ';
        	$this->id = '';
        	$this->style = '';
        	$this->ArrayBody = array();
        	$this->ArrayHead = array();
        }
        
}
?>