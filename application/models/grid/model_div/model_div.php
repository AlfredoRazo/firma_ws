<?php 
class Model_div extends CI_Model{
		var $div;
        var $id;
        var $name;
        var $class;
        var $arrayElement;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
                       
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
       
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
 		public function buildDiv(){
        	$this->div.= $this->getId(). $this->getClass() . $this->getName() . '>';
        	foreach ($this->arrayElement as $value){
        		$this->div.= $value;
        	}
        	$this->div.='</div>';
        }

        public function getDiv(){
        	return $this->div;
        }
        
        public function initElements(){
        	$this->div = ' <div ';
        	$this->id = '';
        	$this->name = '';
        	$this->class = '';
        	$this->arrayElement = array();
        }
        
}
?>