<?php 
class Model_article extends CI_Model{
		var $article;
        var $id;
        var $name;
        var $class;
        var $arrayElement;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
                       
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
       
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
 		public function buildArticle(){
        	$this->article.= $this->getId(). $this->getClass() . $this->getName() . '>';
        	foreach ($this->arrayElement as $value){
        		$this->article.= $value;
        	}
        	$this->article.='</article>';
        }

        public function getArticle(){
        	return $this->article;
        }
        
        public function initElements(){
        	$this->article = ' <article ';
        	$this->id = '';
        	$this->name = '';
        	$this->class = '';
        	$this->arrayElement = array();
        }
        
}
?>