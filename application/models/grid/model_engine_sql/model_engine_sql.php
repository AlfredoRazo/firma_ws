<?php 
class Model_engine_sql extends CI_Model {
		var $TABLE_NAME;
		var $MAX_ROWS;
		var $MIN_ROWS;
		var $PARAMETERS;
		var $DB;
		var $GGLOBAL;
		var $ORDER;
		var $WHERE;
		var $RELATION;
		var $PARENT;
		var $PROPERTIES;
		var $ERRORSQL = true;
		var $COLUMNS_NAME_COMPLEMENT; /*var only info by the grid*/
		function __construct() {
                parent::__construct();
        } 
        
        public function sDB($DB){
        	$this->DB = $DB;
        }
        
        /* 
         * Set Tables, init components
         * */
        public function table($TABLE_NAME){
        	$this->TABLE_NAME = $TABLE_NAME;
        	$this->gComponents();
        }
        
        public function properties($PROPERTIES){
        	$this->PROPERTIES = $PROPERTIES;
        }
        
        private function gComponents(){
        	$this->aTable();
        	if($this->ERRORSQL)
        		$this->gGlobal();
        	else echo "error";
        }
        
        /*
         * Set properties Tables
         * */
        
        private function aTable(){
        	foreach ($this->TABLE_NAME as &$value){
        		if($this->DB->table_exists($value['TABLE_NAME'])){
        			$field_data = $this->DB->field_data($value['TABLE_NAME']);
        			$value['ALIAS'] = $value['TABLE_NAME'];
        			$value['COLUMNS'] = $field_data;
        			$this->gPKFK($value);
        		}else{
        			$this->ERRORSQL = false;
        		}
        	}
        }
        
        /*
         * Get general Array
         * */
        
        private function gGlobal(){
        	if(count($this->TABLE_NAME) > 0){
        		$this->GGLOBAL = array();
        		$this->GGLOBAL['TABLE_NAME'] = $this->gTable();
        		$this->GGLOBAL['COLUMNS'] = $this->gColumns();
        		$this->GGLOBAL['FOREIGN_KEY'] = $this->gFK();
        		$this->TABLE_NAME['general'] =  $this->GGLOBAL;
        	}
        }
        
        /* 
         * Get Primary Key and Foreign Key 
         * */
        
        private function gPKFK(&$value){
        	
        	$table_name = explode('.', $value['TABLE_NAME']);
        	
        	if(count($table_name) > 1){
        		$table_name = $table_name[1];
        	}else{
        		$table_name = $table_name[0];
        	}
        	
        	$query = "SELECT cols.table_name, cols.column_name, cols.position, cons.status, cons.owner,cons.constraint_type ".
        			"FROM all_constraints cons, all_cons_columns cols ".
        			"WHERE cons.constraint_name = cols.constraint_name ".
        			"AND cons.owner = cols.owner AND cols.table_name='".$table_name."' ".
        			"ORDER BY cols.table_name, cols.position";
        	
        	$data = $this->DB->query($query)->result_array();
        	if(!empty($data)){
        		foreach ($data as $valuePKFK){
        			if($valuePKFK['CONSTRAINT_TYPE'] == 'P'){
        				$value['PRIMARY_KEY'][$valuePKFK['COLUMN_NAME']] = $valuePKFK['COLUMN_NAME'];
        				
        			}else if($valuePKFK['CONSTRAINT_TYPE'] == 'R'){
        				$value['FOREIGN_KEY'][$valuePKFK['COLUMN_NAME']] = $valuePKFK['COLUMN_NAME'];
        			}
        		}
        	}
        	
        	
        	
        }
        
        /* 
         * Get Foreign Key general array
         * */
        private function gFK(){
        	$foreignkey = array();
        	foreach ($this->TABLE_NAME as $valueT){
        		if(isset($valueT['PRIMARY_KEY'])){
        			foreach ($valueT['PRIMARY_KEY'] as $valueFK){
        				$cValueP = $valueT['ALIAS'].'.'.$valueFK;
        				$foreignkey[$cValueP] = $cValueP;
        			}
        		}
        		if(isset($valueT['FOREIGN_KEY'])){
        			foreach ($valueT['FOREIGN_KEY'] as $valueF){
        				$cValueF = $valueT['ALIAS'].'.'.$valueF;
        				$foreignkey[$cValueF] = $cValueF;
        			}
        		}
        	}
        	return $foreignkey;
        }
        
        private function pColumns(){
        	foreach ($this->TABLE_NAME as &$valueT){
        		foreach ($valueT['COLUMNS'] as &$valueC){
        			$this->cParameters($valueC);
        			if(isset($this->PARAMETERS[$valueC->name])){
        				$valueC->value = $this->pDataType($valueC,$this->PARAMETERS[$valueC->name]);
        				$this->PARAMETERS[$valueC->name] = $valueC->value;
        			}
        		}
        	}
        }
        
        private function cParameters($valueC){
        	if(isset($this->PARAMETERS[$valueC->name]))
	        	if (strpos($this->PARAMETERS[$valueC->name],'|') !== false ){
	        		$value = explode('|',$this->PARAMETERS[$valueC->name]);
	        		foreach ($value as $valueP){
	        			$pValue = explode('=',$valueP);
	        			if(count($pValue) > 0)
	        				$this->PARAMETERS[$pValue[0]] = $pValue[1];
	        			else
	        				$this->PARAMETERS[$pValue[0]] = '';
	        		}
	        	}
        }
        
        private function pDataType($valueC,$valueP){
        	if($valueC->type == 'DATE' ){
        		if($this->eDate($valueP)){
        			return "to_date('$valueP','YYYY-MM-DD')";
        		}else if($valueP == 'SYSDATE')
        			return "SYSDATE";
        			else
        			return "''";
        	}else if(strpos($valueC->type,'TIMESTAMP') !== false){
        		if($this->eTimeStamp($valueP)){
        			return "to_timestamp('$valueP','YYYY-MM-DD HH24:MI')";
        		}else if($valueP == 'SYSDATE')
        			return "SYSDATE";
        			else
        				return "''";
        	}else{
        		if(strpos($valueP, "'")!== FALSE)
        			return "".$valueP."";
        		else
        			return "'".$valueP."'";
        	}
        }
        
        private function pSelect(&$valueT){
        	$columns = "";
        	$this->aColumns($valueT);
        	
        	foreach ($valueT['COLUMNS'] as $valueC){
        		$columns.=$valueC->value_type.",";
        	}
        	
        	$columns = substr($columns, 0, -1);
        	
        	$this->cColumns($valueT);
        	
        	if(isset($valueT['GENERAL']) && isset($this->TABLE_NAME['general']['complement_column']) && count($this->TABLE_NAME['general']['complement_column'])>0 ){$columns.= ','.$this->bColumns();}
        	
        	$where = $this->pWhere($valueT);
        	$query = "SELECT ".$columns.",count(*) over() cnt,row_number() over(order by 1) rn  FROM ".$valueT['TABLE_NAME'].$where;
        	$valueT['select'] = $query;
        	$query = "SELECT  count(*) as COUNT FROM ".$valueT['TABLE_NAME'].$where;
        	$valueT['count'] = $query;
        	$valueT['where'] = $where;
        	
        }
        
        /*Shows the Columns but does not insert value*/
        
        private function cColumns($valueT){
        	$columns = "";
			if(isset($valueT['CATALOG']))
        		if(isset($valueT['CATALOG']['SHOW_GRID_COLUMNS'])){
		        	foreach ($valueT['CATALOG']['SHOW_GRID_COLUMNS'] as $valueC){
		        		$columns.=$valueT['TABLE_NAME'].'.'.$valueC.',';
		        		$this->COLUMNS_NAME_COMPLEMENT[$valueC] = $valueC;
		        	}
		        	$columns = substr($columns, 0, -1);
		        	if(isset($this->TABLE_NAME['general']['complement_column']))
		        		$this->TABLE_NAME['general']['complement_column']= array($valueT['TABLE_NAME']=>$columns);
		        	else 
		        		$this->TABLE_NAME['general']['complement_column']=  array($valueT['TABLE_NAME']=>$columns);
        		}
        }
        
        /*Build complement columns*/
        
        private function bColumns(){
        	$columns = "";
        	if(isset($this->TABLE_NAME['general']))
        		if (isset($this->TABLE_NAME['general']['complement_column'])) {
        			foreach ($this->TABLE_NAME['general']['complement_column'] as $cColumn){
        				$columns.= $cColumn;
        			}
        		}
        		
        	return $columns;
        }
        
        private function gLimit($query){
        	$fWhere = "";
        	$sWhere = "";
        
        	if((isset($this->MAX_ROWS) && strlen($this->MAX_ROWS)>0 ) && (isset($this->MIN_ROWS) && strlen($this->MIN_ROWS)>0)){
        		$fWhere = $this->MIN_ROWS;
        		$sWhere = $this->MAX_ROWS;
        		$query = "SELECT *FROM ($query) WHERE rn BETWEEN $fWhere AND $sWhere";
        	}else{
        		$query = "SELECT *FROM ($query)";
        	}
        	
        	return $query;
        }
        
        public function gResult(){
        	return $this->TABLE_NAME;
        }
        
        private function gColumns(){
        	$columns = array();
        	foreach ($this->TABLE_NAME as $valueT){
        		foreach ($valueT['COLUMNS'] as $valueC){
        			$cValueC = new stdClass();
        			$cValueC->name = $valueT['ALIAS'].'.'.$valueC->name;
        			$cValueC->type = $valueC->type;
        			$cValueC->max_length = $valueC->max_length;
        			if(isset($valueT['CATALOG'])){
        				if(isset($valueT['PRIMARY_KEY']))
        					foreach ($valueT['PRIMARY_KEY'] as $key => $value){
        						if ($key == $valueC->name){
        							$columns[$valueC->name] = $cValueC;
        						}
        					}
        			}else
        				$columns[$valueC->name] = $cValueC;
        		}
        	}
        	return $columns;
        }
        
        private function gTable(){
        	$table = "";
        	foreach ($this->TABLE_NAME as $valueT){
        		$table.=$valueT['TABLE_NAME'].',';
        	}
        	$table = substr($table, 0, -1);
        	return $table;
        }
        
        private function aColumns(&$valueT){
        	foreach ($valueT['COLUMNS'] as &$valueC){
        		$valueC->value_type = $this->aDataType($valueC,$valueC->name);
        	}
        	 
        }
        
        private function aDataType($valueC,$valueP){
        	if($valueC->type == 'DATE'){
        		$pos = strrpos($valueP,".") ;
        		
        		$value = explode('.',$valueP);
        		
        		if(count($value) == 3){
        			$value = $value[2];
        		}else if(count($value) == 2){
        			$value = $value[1];
        		}else if(count($value) == 1){
        			$value = $value[0];
        		}
        		
        		return "to_char(".$valueP.",'YYYY-MM-DD') as ".$value;
        	}else if(strpos($valueC->type,'TIMESTAMP') !== false){
        		
        		$pos = strrpos($valueP,".") ;
        		
        		$value = explode('.',$valueP);
        		
        		if(count($value) == 3){
        			$value = $value[2];
        		}else if(count($value) == 2){
        			$value = $value[1];
        		}else if(count($value) == 1){
        			$value = $value[0];
        		}
        		
        		return "to_char(".$valueP.",'YYYY-MM-DD HH24:MI') as ".$value;
        	}else{
        		return $valueP;
        	}
        }
        
        
        private function eDate($date){
        	if(preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
        		return true;
        	else
        		return false;
        }
        
        private function eTimeStamp($date){
        	if(preg_match("/^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}$/", $date))
        		return true;
        	else
        		return false;
        	 
        }
        
        private function gSelect(){
        	if(count($this->TABLE_NAME) > 0){
        		$this->TABLE_NAME['general']['GENERAL'] = true;
        		$this->pSelect($this->TABLE_NAME['general']);
        		$this->gRelation();
        		if(strlen($this->WHERE)>0){
        			$where = $this->WHERE;
        		}else{
        			$where = ' ';
        		}
        		$query = $this->gLimit($this->TABLE_NAME['general']['select']." ".$this->RELATION." ".$this->cWhere($this->TABLE_NAME['general']).$where." ".$this->ORDER);
        		$this->TABLE_NAME['general']['select'] = $query;
        		$query = $this->TABLE_NAME['general']['count']." ".$this->RELATION.$this->cWhere($this->TABLE_NAME['general']).$where;
        		$this->TABLE_NAME['general']['count'] = $query;
        	}
        }
        	
        private function pWhere($valueT){
        	$where = "";
        	
        	if(!empty($this->PARAMETERS)){
        		if(isset($valueT['PRIMARY_KEY'])){
        				foreach ($valueT['PRIMARY_KEY'] as $valuePK)
        					if(isset($this->PARAMETERS[$valuePK])){
        						$where.= " ".$valueT['PRIMARY_KEY'][$valuePK]."=".$this->PARAMETERS[$valuePK]." AND ";
	        					//unset($this->PARAMETERS[$valuePK]);
	        				}else if(isset($this->PARAMETERS[$valueT['TABLE_NAME'].'.'.$valuePK])){
	        					$where.= " ".$valueT['PRIMARY_KEY'][$valuePK]."=".$this->PARAMETERS[$valueT['TABLE_NAME'].'.'.$valuePK]." AND ";
	        				}
        		}
        		
        		if(isset($valueT['FOREIGN_KEY'])){
        			foreach ($valueT['FOREIGN_KEY'] as $valueF){
        				if(isset($this->PARAMETERS[$valueF])){
        					$where.= " ".$valueF."=".$this->PARAMETERS[$valueF]." AND ";
        					//unset($this->PARAMETERS[$valueF]);
        				}else if(isset($this->PARAMETERS[$valueT['TABLE_NAME'].'.'.$valueF])){
        					$where.= " ".$valueF."=".$this->PARAMETERS[$valueT['TABLE_NAME'].'.'.$valueF]." AND ";
        				}
        			}
        		}
        			
        		$where = substr($where, 0, -4);
        			
        		if(strlen($where)>0){
        			$where = " WHERE ".$where;
        		}
        	}
        	
        	return $where;
        	
        }
        	
        private function cWhere($valueT){
        	$where = "";
        	if(!empty($this->PARAMETERS)){
        		foreach ($this->PARAMETERS as $key => $value){
        			$where.= " ".$key."=".$value." AND ";
        			//unset($this->PARAMETERS[$key]);
        		}
        	}
        
        	$where = substr($where, 0, -4);
        		
        	if(empty($valueT['where']) && empty($this->RELATION) && strlen($where)>0){
        		$where = " WHERE ".$where;
        	}else if(strlen($where)>0){
        		$where = " AND ".$where;
        	}
        	return $where;
        }
        	
        private function gRelation(){
        	$foreign_key = $this->TABLE_NAME['general']['FOREIGN_KEY'];
        	foreach ($foreign_key as $value){
        		foreach ($foreign_key as $valueKey){
        			$valueColumn = $value;
        			$valueColumnKey = $valueKey;
        			$aColumn = explode('.',$value);
        			$aColumnKey = explode('.',$valueKey);
        			
        			$j = 0;
        			
        			if(count($aColumn) == 3)
        				$j++;
        			
        			$column = $aColumn[$j+1];
        			$columnKey = $aColumnKey[$j+1];
        			if($column == $columnKey && $aColumn[$j+0]!=$aColumnKey[$j+0]){
        				$this->RELATION.=$valueColumn.'='.$valueColumnKey.' AND ';
        			}
        		}
        	}
        
        	$this->RELATION = substr($this->RELATION,0,-4);
        
        	if(empty($this->TABLE_NAME['general']['where']) && !empty($this->RELATION)){
        		$this->RELATION = " WHERE ".$this->RELATION;
        	}else if(!empty($this->RELATION)){
        		$this->RELATION = " AND ".$this->RELATION;
        	}else{
        		$this->RELATION = "";
        	}
        }
        
        public function rowNum($start='',$end=''){
        	if(strlen($start)>0){
        		$this->MIN_ROWS = $start;
        	}
        	if(strlen($end)>0){
        		$this->MAX_ROWS = $end;
        	}
        }
        
        public function where($where){
        	$this->WHERE = " WHERE ".$where;
        }
        
        public function order($ORDER){
        	$this->ORDER = $ORDER;
        }
        
        public function select($parameters = array()){
        	$this->PARAMETERS = $parameters;
        	$this->pColumns();
        	foreach ($this->TABLE_NAME as &$valueT){
        		$this->pSelect($valueT);
        	}
        	$this->gSelect();
        }

        public function delete($parameters = array()){
        	$this->PARAMETERS = $parameters;
        	$this->pColumns();
        	foreach ($this->TABLE_NAME as $key =>&$valueT){
        		if(!isset($valueT['CATALOG']) && $key !=='general')
        			$this->pDelete($valueT);
        	}
        }
        
        private function pDelete(&$valueT){
        	$columns = "";
        	$this->aColumns($valueT);
        	foreach ($valueT['COLUMNS'] as $valueC){
        		$columns.=$valueC->value_type.",";
        	}
        	$columns = substr($columns, 0, -1);
        	$where = $this->pWhere($valueT);
        	
        	$query = "DELETE  FROM ".$valueT['TABLE_NAME'].$where;
        	$valueT['delete'] = $query;
        }
        
        public function insert($parameters){
        	$this->PARAMETERS = $parameters;
        	$this->pColumns();
        	 
        	foreach ($this->TABLE_NAME as $key => &$valueT){
        		$this->pSelect($valueT);
        		$this->pInsert($valueT);
        	}
        }
        
        private function pInsert(&$valueT){
        	if(isset($valueT['PRIMARY_KEY']) && !isset($valueT['CATALOG'])){
         		$query = "INSERT INTO ";
       		    $query.= $valueT['TABLE_NAME']."(";
        		$columns = "";
        		$values = "";
        		foreach ($valueT['COLUMNS'] as $valueC){
	        		if(isset($valueC->value)){
	        			$columns.=$valueC->name.",";
	        			$values.=$valueC->value.",";
	        		}
        		}
        		$columns = substr($columns, 0, -1);
        		$values = substr($values, 0, -1);
        
        		$query.=$columns.") VALUES(".$values.")";
        		$valueT['insert'] = $query;
        	}
        }
        
        public function update($parameters){
        	$this->PARAMETERS = $parameters;
        	$this->pColumns();
        	foreach ($this->TABLE_NAME as &$valueT){
        		$this->pUpdate($valueT);
        	}
        }
        
        private function pUpdate(&$valueT){
        	if(isset($valueT['PRIMARY_KEY']) && !isset($valueT['CATALOG'])){
	        	$query = "UPDATE ";
	        	$query.= $valueT['TABLE_NAME']." SET ";
	        	$values = "";
	        	$where = "";
	        	
	        	foreach ($valueT['COLUMNS'] as $valueC){
	        		if(isset($valueC->value) && (!array_key_exists($valueC->name,$valueT['PRIMARY_KEY'])))
	        			$values.=$valueC->name."=".$valueC->value.",";
	        		else if(isset($valueT['PRIMARY_KEY'])){
	        			$where.=$valueT['PRIMARY_KEY'][$valueC->name]."=".$valueC->value." AND ";
	        		}       		
	        	}
	        	
	        	$values = substr($values, 0, -1);
	        	$where = substr($where, 0, -4);
	        	
	        	if(strlen(trim($where))>1){
	        		$where = ' WHERE ' .$where;
	        	}
	
	        	$query.=$values.$where;
	        	
	        	if(!empty($values))
	        		$valueT['update'] = $query;
        	}
        }
        
}
?>