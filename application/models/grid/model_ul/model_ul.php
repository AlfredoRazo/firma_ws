<?php 
class Model_ul extends CI_Model{
        var $ul;
        var $id;
        var $class;
        var $name;
        var $arrayElement;
       
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
        
        
        public function setName($name){
        	$this->name = " name = '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function openLi($class){
        	$this->arrayElement[] = ' <li class= "'.$class.'">';
        } 
        
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
        public function closeLi(){
        	$this->arrayElement[] = '</li>';
        }
        
        public function buildUl(){
        	$this->ul.= $this->getId() . $this->getName() . $this->getClass() . '>';
        	
        	foreach ($this->arrayElement as $value){
        		$this->ul.= $value;
        	}
        	
        	$this->ul.= '</ul>';
        }

        public function getul(){
        	return $this->ul;
        }
        
        public function initElements(){
        	$this->ul = ' <ul ';
        	$this->id = '';
        	$this->name = '';
        	$this->arrayElement = array();
        }
        
}
?>