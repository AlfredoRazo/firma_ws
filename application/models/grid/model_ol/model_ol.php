<?php 
class Model_ol extends CI_Model{
        var $ol;
        var $id;
        var $class;
        var $name;
        var $arrayElement;
       
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
        
        
        public function setName($name){
        	$this->name = " name = '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function openLi($class){
        	$this->arrayElement[] = ' <li class= "'.$class.'">';
        } 
        
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
        public function closeLi(){
        	$this->arrayElement[] = '</li>';
        }
        
        public function buildOl(){
        	$this->ol.= $this->getId() . $this->getName() . $this->getClass() . '>';
        	
        	foreach ($this->arrayElement as $value){
        		$this->ol.= $value;
        	}
        	
        	$this->ol.= '</ol>';
        }

        public function getOl(){
        	return $this->ol;
        }
        
        public function initElements(){
        	$this->ol = ' <ol ';
        	$this->id = '';
        	$this->name = '';
        	$this->arrayElement = array();
        }
        
}
?>