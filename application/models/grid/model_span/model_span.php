<?php 
class Model_span extends CI_Model{
        var $span;
        var $id;
        var $class;
        var $name;
        var $arrayElement;
       	function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
        
        
        public function setName($name){
        	$this->name = " name = '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function addElement($element){
        	$this->arrayElement[] = $element;
        } 
        
        public function buildSpan(){
        	$this->span.= $this->getId() . $this->getName() . $this->getClass() . '>';
        	
        	foreach ($this->arrayElement as $value){
        		$this->span.= $value;
        	}
        	
        	$this->span.= '</span>';
        }

        public function getSpan(){
        	return $this->span;
        }
        
        public function initElements(){
        	$this->span = ' <span ';
        	$this->id = '';
        	$this->name = '';
        	$this->arrayElement = array();
        }
        
}
?>