<?php 
class Model_label extends CI_Model{
		var $label;
        var $id;
        var $name;
        var $class;
        var $for;
        var $arrayElement;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }

        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
        
        public function setFor($for){
        	$this->for = " for= '".$for."' ";
        }
        
        public function getFor(){
        	return $this->for;
        }
        
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
        
 		public function buildLabel(){
        	$this->label.= $this->getId(). $this->getClass() . $this->getName(). $this->getFor() . '>';
        	foreach ($this->arrayElement as $value){
        		$this->label.= $value;
        	}
        	$this->label.='</label>';
        }
        
        public function getLabel(){
        	return $this->label;
        }
        
        public function initElements(){
        	$this->label = ' <label ';
        	$this->id = '';
        	$this->name = '';
        	$this->class = '';
        	$this->for = '';
        	$this->arrayElement = array();
        }
        
}
?>