<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exp_reg extends CI_Model{

	function __construct(){
		parent::__construct();
		
	}
	

	public function control($tipo,$size){
		
		$expReg = array('T'=> '/^[a-zA-Z������������������������_\s]{'.$size.'}+$/',
						'N'=> '/^[0-9]{'.$size.'}+$/', 
						'A'=> '/^[A-Z������������������������_\s][0-9]{'.$size.'}$/',
						'D'=> '/(\d{1,2}\/\d{1,2}\/\d{4})/',
						'M'=> '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/',
						'P'=> '/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/');
		return $expReg[$tipo];
		}
	
}


?>