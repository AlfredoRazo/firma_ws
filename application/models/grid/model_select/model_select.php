<?php 
class Model_select extends CI_Model{
        var $select;
        var $id;
        var $name;
        var $value;
        var $ArrayOption;
       
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name = '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function openOption($value,$display,$selected){
        	$this->ArrayOption[] = ' <option value= "'.$value.'" '.$selected.'>'.$display.'';
        } 
        
        public function closeOption(){
        	$this->ArrayOption[] = '</option>';
        }
        
        public function buildSelect(){
        	$this->select.= $this->getId() . $this->getName() . '>';
        	
        	
        	foreach ($this->ArrayOption as $value){    
        		$this->select.= $value;
        	}
        	
        	$this->select.= '</select>';
        	 
        	
        }

        public function getSelect(){
        	return $this->select;
        }
        
        public function initElements(){
        	$this->select = ' <select data-placeholder="Choose a Country..." class="chosen-select" style="width:350px;" tabindex="2" ';
        	$this->id = '';
        	$this->name = '';
        	$this->value = '';
        	$this->ArrayOption = array();
        	//$this->ArrayOption[] = '<option value=""></option>';
        }
        
}
?>