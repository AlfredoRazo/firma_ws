<?php 
class Model_input extends CI_Model{
		var $type;
		var $value;
		var $class;
		var $maxlength;
        var $input;
        var $id;
        var $name;
        var $min;
        var $max;
       	var $onclick;
       	var $disable;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        public function setType($type){
        	$this->type = " type= '".$type."'";
        }
        
        public function getType(){
        	return $this->type;
        }
        
        public function setValue($value){
        	$this->value = " value= '".$value."'";
        }
        
        public function getValue(){
        	return $this->value;
        }
        
        public function setClass($class){
        	$this->class = " class = '".$class."'";
        	
        }
        
        public function getClass(){
        	return $this->class;
        	 
        }
        
        public function setMaxlength($maxLen){
        	$this->maxlength = " maxlength = ".$maxLen."";        	
        	
        }
        
        public function setOnclick($onclick){
        	$this->onclick = " onclick='" . $onclick . "' ";
        }
        
        public function getOnclick(){
        	return $this->onclick;
        }
        
        public function getMaxlength(){
        	return $this->maxlength;
        	 
        }
                 
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function setMin($min){
        	$this->min = " min= '".$min."'";
        }
        
        public function getMin(){
        	return $this->min;
        }
        
        public function setMax($max){
        	$this->max = " max= '".$max."'";
        }
        
        public function getMax(){
        	return $this->max;
        }
        
        public function getInput(){
        	return $this->input;
        }
        
        public function setDisable(){
        	$this->disable = ' disable ';
        }
        
        public function getDisable(){
        	return $this->disable;
        }
        
 		public function buildInput(){
        	$this->input.= $this->getType() . $this->getValue() . $this->getClass() . $this->getMaxlength() . $this->getId() . $this->getName() . $this->getMin() . $this->getMax() . $this->getOnclick() . $this->getDisable() . '>';
        }
        
        
        public function initElements(){
        	$this->input = ' <input ';
        	$this->value = '';
        	$this->class = '';
        	$this->maxlength = '';
        	$this->id = '';
        	$this->name = '';
        	$this->min = '';
        	$this->max = '';
        	$this->onclick = '';
        	$this->disable = '';
        }
        
}
?>