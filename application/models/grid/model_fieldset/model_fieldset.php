<?php 
class Model_fieldset extends CI_Model{
		var $fieldset;
        var $id;
        var $name;
        var $class;
        var $arrayElement;
		function __construct() {
            parent::__construct();
        	$this->initElements();
        }
        
        
        public function setId($id){
        	$this->id = " id= '".$id."'";
        }
        
        public function getId(){
        	return $this->id;
        }
        
        public function setName($name){
        	$this->name = " name= '".$name."'";
        }
        
        public function getName(){
        	return $this->name;
        }
        
        public function setClass($class){
        	$this->class = " class= '".$class."'";
        }
        
        public function getClass(){
        	return $this->class;
        }
       
        public function addElement($value){
        	$this->arrayElement[] = $value;
        }
        
 		public function buildFieldset(){
        	$this->fieldset.= $this->getId(). $this->getClass() . $this->getName() . '>';
        	foreach ($this->arrayElement as $value){
        		$this->fieldset.= $value;
        	}
        	$this->fieldset.='</fieldset>';
        }
        
        public function getFieldSet(){
        	return $this->fieldset;
        }
        
        public function initElements(){
        	$this->fieldset = ' <fieldset ';
        	$this->id = '';
        	$this->name = '';
        	$this->class = '';
        	$this->arrayElement = array();
        }
        
}
?>