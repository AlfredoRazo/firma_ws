<?php  
class Model_firma_electronica extends CI_Model{
	
	/**
	
	 
	@brief Model_firma_electronica
	 
	@author Guillermo Omar Martinez Toledo
	@date 18/04/2015, 25/04/2015
	 
	*/
	
	var $KEY = "hUgpOw2IgnPms1PFHU2I/v5vPgk4UwDr";
	

	/**
	 @brief Verifica la llave publica del contribuyente
	 @brief Verifica si la llave publica es vigente
	 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error
	 @returns JSON result
	 */

	public function revisaSATNoSave($file_encode,$key){
	
		if($this->validateKey($key)){
			$certificateCAcerContent = $this->dCrypt($file_encode);
			
			$certificateCApemContent = chunk_split(base64_encode($certificateCAcerContent), 64, PHP_EOL);
			
			$servidor = "http://sfe.finanzas.df.gob.mx:8080/GDFConsultaOCSPSAT/WSConsultarEstadoCertificado?wsdl";
			
			$pregunta = new stdClass();
			$pregunta->certificadoDigital = $certificateCApemContent;
			
			$client = new SoapClient($servidor);
			
			$result = $client->consultarEstadoCertificado($pregunta);

			if(!empty($result)){

			$return = new stdClass();
			$return = $result->return;
			
			$info = array ();
			
			if($return[0] == 1){
				$info['mensaje'] = "Acceso Permitido";
			}else{
				$info['error'] = $return[0];
				$info['mensaje'] = "Acceso Negado";
			}
		 }else{
		 	$info['error'] = true;
			$info['mensaje'] = "No se pudo comunicar con el servicio del SAT intentelo más tarde";

		 }
		}else{
			$info['error'] = 'Llave de Comunicacion incorrecta';
			$info['mensaje'] = "Acceso Negado ";
		}
		return $info;
	}

	
	
	/**
		 @brief Obtiene los datos de la llave publica
		 @brief Retorna el valor en cadena
		 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error
		 @returns JSON result
		 */
	
	public function getDataCetificateComplete($file_encode,$key){
		
		if($this->validateKey($key)){
		
			$nombre_fichero_var_file_tmp = tempnam("/tmp", $this->createVarUserNameFile());
			
			$actual = file_get_contents($nombre_fichero_var_file_tmp);
			
			$actual .= $this->dCrypt($file_encode);
			
			file_put_contents($nombre_fichero_var_file_tmp,$actual);
			
			$nombre_fichero_tmp = tempnam("/tmp", $this->createVarUserNameFile());
			
			shell_exec('openssl x509 -inform DER -outform PEM -in '.$nombre_fichero_var_file_tmp.' -pubkey -out ' .$nombre_fichero_tmp.'.pem');
			
			$out = shell_exec('openssl x509 -in '.$nombre_fichero_tmp.'.pem'.' -noout -text');
			
			unlink($nombre_fichero_tmp);
			unlink($nombre_fichero_tmp.'.pem');
			unlink($nombre_fichero_var_file_tmp);
			
			if($out!=NULL){
				$info['mensaje'] = $out;
				return $info;
			}else{
				$info['error'] = 'Ocurrio un problema con el archivo cer';
				$info['mensaje'] = "Error en archivo Cer";
			}
			
		}else{
			$info['error'] = 'Llave de Comunicacion incorrecta';
			$info['mensaje'] = "Acceso Negado ";
		}
		
	}
	
	/**
		 @brief Obtiene los datos de la llave publica del contribuyente
		 @brief Retorna el valor en arreglo
		 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error
		 @returns JSON result
		 */
	
	
	public function getDataCetificateSubject($file_encode,$key){
	
		if($this->validateKey($key)){
		
			$nombre_fichero_var_file_tmp = tempnam("/tmp", $this->createVarUserNameFile());
				
			$actual = file_get_contents($nombre_fichero_var_file_tmp);
				
			$actual .= $this->dCrypt($file_encode);
				
			file_put_contents($nombre_fichero_var_file_tmp,$actual);
				
			$nombre_fichero_tmp = tempnam("/tmp", $this->createVarUserNameFile());
				
			shell_exec('openssl x509 -inform DER -outform PEM -in '.$nombre_fichero_var_file_tmp.' -pubkey -out ' .$nombre_fichero_tmp.'.pem');
				
			$out = shell_exec('openssl x509 -in '.$nombre_fichero_tmp.'.pem'.' -noout -subject');
				
			unlink($nombre_fichero_tmp);
			unlink($nombre_fichero_tmp.'.pem');
			unlink($nombre_fichero_var_file_tmp);
			
	

			if($out!=NULL){
				
				$out = str_replace('subject= /', '', $out);
				
				$data = explode('/', $out);
				
				$obj = new stdClass();
				foreach ($data as $value){
					$val = explode('=', $value);
					if(isset($val[1]))
					 $obj->$val[0] = $val[1];
				}
				
				$info['mensaje'] = (array)$obj;
				return $info;
			
			}else{
				$info['error'] = 'Ocurrio un problema con el archivo cer';
				$info['mensaje'] = "Error en archivo Cer";
			}
			
		}else{
			$info['error'] = 'Llave de Comunicacion incorrecta';
			$info['mensaje'] = "Acceso Negado ";
		}
		
		
		return $info;
		
	}
	
	/**
	 @brief Crea variable de usuario por Ymd his
	 @returns num_key_cer
	 */
	
	private function createVarUserNameFile(){
		$num_key_cer = rand(0, 10000) . date('Ymd') . date('his');
		return $num_key_cer;
	}
	
	/**
	 @brief Crea el sello digital
	 @brief Regresa la cadena sellada
	 @brief Regresa Objeto JSON, si la llave es correcta regresa la variable mensaje, si la llave no es correcta regresa las variables mensaje y error
	 @returns JSON result
	 */
	
	public function buildSelloDigital($file_encode,$key,$cer,$password,$cadenaOriginal){
	
		if(!empty($cadenaOriginal)){
			if($this->validateKey($key)){
				
				//Key 
				$nombre_fichero_var_file_tmp = tempnam("/tmp", $this->createVarUserNameFile());
				
				$actual = file_get_contents($nombre_fichero_var_file_tmp);
				
				$actual .= $this->dCrypt($file_encode);
				
				$password = $this->dCrypt($password);
				
				if(strpos($password," ") !== false){
					$password = escapeshellarg($password);
				}else{
					$password = escapeshellcmd($password);
				}
				
				file_put_contents($nombre_fichero_var_file_tmp,$actual);
				
				$llavePrivada = shell_exec('openssl pkcs8 -inform DER -in '.$nombre_fichero_var_file_tmp.' -passin pass:'.$password);
				
				
				//Cer
				$nombre_fichero_var_file_tmp_cer = tempnam("/tmp", $this->createVarUserNameFile());
				
				$actual = file_get_contents($nombre_fichero_var_file_tmp_cer);
				
				$actual .= $this->dCrypt($cer);
				
				file_put_contents($nombre_fichero_var_file_tmp_cer,$actual);
				
				$llavePublica = shell_exec('openssl x509 -inform DER -outform PEM -in '.$nombre_fichero_var_file_tmp_cer.' -pubkey ');
				
				$ClavePrivada = openssl_pkey_get_private($llavePrivada,$password);
				
				$cVerify = false;
				
				openssl_x509_check_private_key(
				$llavePublica,
				$ClavePrivada)
				? $cVerify = true
				: $cVerify = false;
				
				if($cVerify)
					if($llavePrivada!=NULL){
						$cadenaOriginal = $this->buildString($cadenaOriginal);
						$pkeyid = openssl_pkey_get_private($llavePrivada,$password);
						openssl_sign($cadenaOriginal, $crypttext, $pkeyid, OPENSSL_ALGO_SHA1);
						openssl_free_key($pkeyid);
						$sello = base64_encode($crypttext);
						$info['mensaje'] = array('sello'=>$sello,'cadena_signed'=>$cadenaOriginal);
					}else{
						$info['error'] = 'Archivo o Password Incorrecto';
						$info['mensaje'] = "Creacion del sello digital Negado ";
					}
				else {
					$info['error'] =  'Esta clave privada no corresponde al certificado';
					$info['mensaje'] = "Creacion del sello digital Negado";
				}
				
				unlink($nombre_fichero_var_file_tmp);
				unlink($nombre_fichero_var_file_tmp_cer);
				
			}else{
				$info['error'] = 'Llave de Comunicacion incorrecta';
				$info['mensaje'] = "Acceso Negado ";
			}
		}else{
			$info['error'] = 'La cadena es vacia no se genero sello';
			$info['mensaje'] = "Acceso Negado ";
		}
		
		return $info;	
	}
	
	/**
	 @brief Prepara el string para ser sellado
	 @returns String
	 */
	
	public function buildString($cadenaOriginal){
		$cOriginal = '||';
		$aOriginal = explode(' ', $cadenaOriginal);
		foreach ($aOriginal as $value){
			$cOriginal.=trim($value).'|';
		}
		$cOriginal.='|';
		return $cOriginal;
	}
	
	
	/**
	 @brief Valida Key
	 @returns boolean
	 */
	
	private function validateKey($key){
		if($this->KEY == $key){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 @brief Encode
	 @returns String Encode
	 */
	
	private function mCrypt($plaintext = ''){
			
		$key = $this->KEY;
			
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,$plaintext, MCRYPT_MODE_CBC, $iv);
			
		$ciphertext = $iv . $ciphertext;
			
		$ciphertext_base64 = base64_encode($ciphertext);
			
		return $ciphertext_base64;
			
	}
	
	/**
	 @brief Decode
	 @returns String Decode
	 */
	
	private function dCrypt($ciphertext_base64){
			
		$key = $this->KEY;
			
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			
		$ciphertext_dec = base64_decode($ciphertext_base64);
			
		$iv_dec = substr($ciphertext_dec, 0, $iv_size);
			
		$ciphertext_dec = substr($ciphertext_dec, $iv_size);
			
		$plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,$ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
			
		return $plaintext_dec;
	}


}



?>
