<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Controller extends CI_Controller{
	var $PATH;
	var $CLASS;
	function __construct(){
		parent::__construct();
		
		
	}
	public function sClass($class){
		$this->CLASS = $class;
	}
	public function sPATH($path){
		$this->PATH = $path;
	}
	
	public function gPath(){
		$path = explode('controllers/',$this->PATH);
		if(count($path) > 1){$path = $path[1];}
		if(is_array($path)){$path = strtolower($this->CLASS);
		}else{$path.= '/'.strtolower($this->CLASS);}
		return $path;
	}
	
	public function index(){

	} 

	public function gGrid($eS){
		$name = $eS->ID;
		$html = $eS->buildTable($name);
		$flex = $eS->buildFlex($name);
		$eS->initFuntions();
		$box = '<script>function target_popup'.$name.'(form) { var jsonData = {}; '.
			   'for (var i=0; i<form.length; i++){'.
			   'if(form.elements[i].value != null){'.
			   'jsonData[form.elements[i].name] = form.elements[i].value;'.
			   '}'.
			   '}'.
			   'var url = form.action;'.
			   'var posting = $.post( url,jsonData);'.
			   'posting.done(function( data ) {'.
			   '$( "#result" ).empty().append( data );'.
			   '$( "#result" ).dialog({width: 600,height: 500,modal:true, buttons: [{text:"Guardar",click: function() { var form = $("#gridForm"); var vUrl = form.attr("action"); var dataString = form.serialize(); $.ajax({type:"POST",url:vUrl,data:dataString,beforeSend:function(){$(".ui-dialog-content").dialog("close");},success: function(data){ $("#'.$name.'").flexReload(); }}); }}]   });'.
			   '});}</script>';
		
		$boxDelete = '<script>function target_popupDelete'.$name.'(form) {'.
				'$( "#dialog-confirm" ).dialog({resizable:false,height:180,modal:true,buttons:[{text:"Aceptar",click:function(){  var vUrl = form.action; var jsonData = {}; for (var i=0; i<form.length; i++){ if(form.elements[i].value != null){ jsonData[form.elements[i].name] = form.elements[i].value; }} var posting = $.post( vUrl,jsonData); posting.done(function( data ) { $(".ui-dialog-content").dialog("close"); $("#'.$name.'").flexReload();});   }}]});'.
				'}</script>';
		

		$boxPDF = '<script>function target_popupPOPUPCONTENT'.$name.'(form) { var jsonData = {}; '.
			   'for (var i=0; i<form.length; i++){'.
			   'if(form.elements[i].value != null){'.
			   'jsonData[form.elements[i].name] = form.elements[i].value;'.
			   '}'.
			   '}'.
			   'var url = form.action;'.
			   'var posting = $.post( url,jsonData);'.
			   'posting.done(function( data ) {'.
			   '$( "#result" ).empty().append( data );'.
			   '$( "#result" ).dialog({width: 800,height: 700,modal:true, buttons: [{text:"Cerrar",click: function() { $(".ui-dialog-content").dialog("close"); }}]   });'.
			   '});}</script>';
		
		
		$viewContent = '<script>function show_content'.$name.'(form){
					 form.submit();
				}</script>';
		
		$deleteDialog = "<div id='dialog-confirm' title='Eliminar' style='display:none;'>".
					   "<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>&iquest;Esta seguro de eliminar el registro?</p>".
					   "</div>";
		
		$result = '<div id="result" ></div>';
		
		if($eS->getShowBackButton()){
			$buttonBack = $eS->buildBackButton();
		}else{
			$buttonBack = '';
		}
		return $html.$buttonBack.$deleteDialog.$result.$box.$boxDelete.$boxPDF.$viewContent.' <script>$(document).ready(function() {'.$flex.'});</script>';
	}

		
	public function edit(){
		$post = $this->input->post();
		$this->load->model('grid/model_engine/model_engine');
		$mE = new Model_engine();
		$this->load->view('grid/body',array('html'=>$mE->edit($post)));
	}
	
	public function create(){
		$post = $this->input->post();
		$this->load->model('grid/model_engine/model_engine');
		$mE = new Model_engine();
		$this->load->view('grid/body',array('html'=>$mE->create($post)));
	}
	
	public function save(){
			$post = $this->input->post();
			$this->load->model('grid/model_engine/model_engine');
			$mE = new Model_engine();
			$mE->save($post);
	}
	
	public function update(){
			$post = $this->input->post();
			$this->load->model('grid/model_engine/model_engine');
			$mE = new Model_engine();
			$mE->update($post);
	}
	
	public function delete(){
		$post = $this->input->post();
		$this->load->model('grid/model_engine/model_engine');
		$mE = new Model_engine();
		return $mE->delete($post);
	}
	
	
}


?>